const nodemailer = require('nodemailer');

const MailingProvider = require('./MailingProvider');
const { mailing } = require('../../config');
const logger = require('../logger');
const { getI18n } = require('../utils/i18n-utils');
const frTranslations = require('../../../translations/fr');

const i18n = getI18n();

function _formatMessage(options) {

  const messageBody = i18n.__(options.template, { options: JSON.stringify(options, undefined, 2)});

  const message = {
    from: options.fromName,
    to: options.to,
    subject: options.subject,
    text: messageBody
  };

  return message;
}

class SmtpProvider extends MailingProvider {
  constructor() {
    super();

    this._transport = nodemailer.createTransport({
      host: mailing.smtp.host,
      port: mailing.smtp.port,
      auth: {
        user: mailing.smtp.user,
        pass: mailing.smtp.password
      }
    });
  }

  sendEmail(options) {
    const message = _formatMessage(options);
    return this._transport.sendMail(message, (err, info) => {
      if (err) {
        logger.error('Smtp send email An error occurred: ' + JSON.stringify(err, undefined, 2));
      } else {
        logger.info('Smtp send email info : ' + JSON.stringify(info, undefined, 2));
      };
    });
  }

}

module.exports = SmtpProvider;
