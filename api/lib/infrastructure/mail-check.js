const { promisify } = require('util');
const resolveMxFn = require('dns').resolveMx;

let resolveMx = promisify(resolveMxFn);

module.exports = {
  checkDomainIsValid(address) {
    const domain = address.replace(/.*@/g, '');
    return resolveMx(domain).then(() => true);
  },
  setResolveMx(newResolvMx) {
    resolveMx = promisify((domain, cb) => {
      try {
        cb(null, newResolvMx(domain));
      } catch (err) {
        cb(err, null);
      }
    });
  },
  clearResolveMx() {
    resolveMx = promisify(resolveMxFn);
  },
};
