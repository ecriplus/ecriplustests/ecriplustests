class Skill {
  constructor({
    id,
    name,
    hint,
    hintStatus = 'no status',
    tutorialIds = [],
    learningMoreTutorialIds = [],
    pixValue,
    competenceId,
    scenariCode,
  } = {}) {
    this.id = id;
    this.name = name;
    this.hint = hint;
    this.hintStatus = hintStatus;
    this.tutorialIds = tutorialIds;
    this.learningMoreTutorialIds = learningMoreTutorialIds;
    this.pixValue = pixValue;
    this.competenceId = competenceId;
    this.scenariCode = scenariCode;
  }

  static getAirtableName() {
    return 'Acquis';
  }

  static getUsedAirtableFields() {
    return [
      'Nom',
      'Indice',
      'Statut de l\'indice',
      'Comprendre',
      'En savoir plus',
      'PixValue',
      'Compétence (via Tube)',
      'Status',
      'scenari_code',
    ];
  }

  static fromAirTableObject(airtableSkillObject) {
    return new Skill({
      id: airtableSkillObject.getId(),
      name: airtableSkillObject.get('Nom'),
      hint: airtableSkillObject.get('Indice'),
      hintStatus: airtableSkillObject.get('Statut de l\'indice'),
      tutorialIds: airtableSkillObject.get('Comprendre'),
      learningMoreTutorialIds: airtableSkillObject.get('En savoir plus'),
      pixValue: airtableSkillObject.get('PixValue'),
      competenceId: airtableSkillObject.get('Compétence (via Tube)')[0],
      scenariCode: airtableSkillObject.get('scenari_code'),
    });
  }
}

module.exports = Skill;
