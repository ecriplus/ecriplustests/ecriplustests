const { PDFDocument, rgb } = require('pdf-lib');
const { readFile } = require('fs/promises');
const pdfLibFontkit = require('@pdf-lib/fontkit');
const moment = require('moment');
const _ = require('lodash');
const bluebird = require('bluebird');
// eslint-disable-next-line no-restricted-modules
const axios = require('axios');

const AttestationViewModel = require('./AttestationViewModel');
const { CertificationAttestationGenerationError } = require('../../../domain/errors');

const fonts = {
  openSansBold: 'OpenSans-Bold.ttf',
  openSansSemiBold: 'OpenSans-SemiBold.ttf',
  robotoMedium: 'Roboto-Medium.ttf',
  robotoMonoRegular: 'RobotoMono-Regular.ttf',
};

const templates = {
  withProfessionalizingCertificationMessageAndWithComplementaryCertifications:
    'withProfessionalizingCertificationMessageAndWithComplementaryCertifications',
  withProfessionalizingCertificationMessageAndWithoutComplementaryCertifications:
    'withProfessionalizingCertificationMessageAndWithoutComplementaryCertifications',
  withoutProfessionalizingCertificationMessageAndWithComplementaryCertifications:
    'withoutProfessionalizingCertificationMessageAndWithComplementaryCertifications',
  withoutProfessionalizingCertificationMessageAndWithoutComplementaryCertifications:
    'withoutProfessionalizingCertificationMessageAndWithoutComplementaryCertifications',
};

async function getCertificationAttestationsPdfBuffer({
  certificates,
  isFrenchDomainExtension,
  dirname = __dirname,
  fontkit = pdfLibFontkit,
  creationDate = new Date(),
} = {}) {
  const viewModels = certificates.map((certificate) => AttestationViewModel.from(certificate, isFrenchDomainExtension));
  const generatedPdfDoc = await _initializeNewPDFDocument(fontkit);
  generatedPdfDoc.setCreationDate(creationDate);
  generatedPdfDoc.setModificationDate(creationDate);
  const embeddedFonts = await _embedFonts(generatedPdfDoc, dirname);
  const embeddedImages = await _embedImages(generatedPdfDoc, viewModels);

  const templatePdfPages = await _embedTemplatePagesIntoDocument(viewModels, dirname, generatedPdfDoc);

  await _render({ templatePdfPages, pdfDocument: generatedPdfDoc, viewModels, rgb, embeddedFonts, embeddedImages });

  const buffer = await _finalizeDocument(generatedPdfDoc);

  const fileName = `attestation-pix-${moment(certificates[0].deliveredAt).format('YYYYMMDD')}.pdf`;

  return {
    buffer,
    fileName,
  };
}

async function _initializeNewPDFDocument(fontkit) {
  const pdfDocument = await PDFDocument.create();
  pdfDocument.registerFontkit(fontkit);
  return pdfDocument;
}

async function _embedFonts(pdfDocument, dirname) {
  const embeddedFonts = {};
  for (const fontKey in fonts) {
    const embeddedFont = await _embedFontInPDFDocument(pdfDocument, fonts[fontKey], dirname);
    embeddedFonts[fontKey] = embeddedFont;
  }
  return embeddedFonts;
}

async function _embedFontInPDFDocument(pdfDoc, fontFileName, dirname) {
  const fontFile = await readFile(`${dirname}/files/${fontFileName}`);
  return pdfDoc.embedFont(fontFile, { subset: true, customName: fontFileName });
}

async function _embedImages(pdfDocument, viewModels) {
  const embeddedImages = {};

  const uniqStickerUrls = _(viewModels)
    .flatMap(({ stickers }) => stickers)
    .map('url')
    .uniq()
    .value();
  await bluebird.each(uniqStickerUrls, async (url) => {
    embeddedImages[url] = await _embedCertificationImage(pdfDocument, url);
  });
  return embeddedImages;
}

async function _embedCertificationImage(pdfDocument, certificationImagePath) {
  let response;
  try {
    response = await axios.get(certificationImagePath, {
      responseType: 'arraybuffer',
    });
  } catch (_) {
    throw new CertificationAttestationGenerationError();
  }
  const [page] = await pdfDocument.embedPdf(response.data);
  return page;
}

async function _embedTemplatePagesIntoDocument(viewModels, dirname, pdfDocument) {
  const templatePages = {};

  if (_atLeastOneWithComplementaryCertifications(viewModels)) {
    if (_atLeastOneWithProfessionalizingCertification(viewModels)) {
      templatePages[templates.withProfessionalizingCertificationMessageAndWithComplementaryCertifications] =
        await _embedFirstPageFromTemplateByFilename(
          'attestation-template-with-professionalizing-message-and-with-complementary-certifications.pdf',
          pdfDocument,
          dirname
        );
    }

    if (_atLeastOneWithoutProfessionalizingCertification(viewModels)) {
      templatePages[templates.withoutProfessionalizingCertificationMessageAndWithComplementaryCertifications] =
        await _embedFirstPageFromTemplateByFilename(
          'attestation-template-without-professionalizing-message-and-with-complementary-certifications.pdf',
          pdfDocument,
          dirname
        );
    }
  }

  if (_atLeastOneWithoutComplementaryCertifications(viewModels)) {
    if (_atLeastOneWithProfessionalizingCertification(viewModels)) {
      templatePages[templates.withProfessionalizingCertificationMessageAndWithoutComplementaryCertifications] =
        await _embedFirstPageFromTemplateByFilename(
          'attestation-template-with-professionalizing-message-and-without-complementary-certifications.pdf',
          pdfDocument,
          dirname
        );
    }

    if (_atLeastOneWithoutProfessionalizingCertification(viewModels)) {
      templatePages[templates.withoutProfessionalizingCertificationMessageAndWithoutComplementaryCertifications] =
        await _embedFirstPageFromTemplateByFilename(
          'attestation-template-without-professionalizing-message-and-without-complementary-certifications.pdf',
          pdfDocument,
          dirname
        );
    }
  }

  return templatePages;
}

async function _embedFirstPageFromTemplateByFilename(templatePdfDocumentFileName, destinationDocument, dirname) {
  const templateBuffer = await _loadTemplateByFilename(templatePdfDocumentFileName, dirname);
  const [templatePage] = await destinationDocument.embedPdf(templateBuffer);
  return templatePage;
}

function _atLeastOneWithComplementaryCertifications(viewModels) {
  return _.some(viewModels, (viewModel) => viewModel.shouldDisplayComplementaryCertifications());
}

function _atLeastOneWithoutComplementaryCertifications(viewModels) {
  return _.some(viewModels, (viewModel) => !viewModel.shouldDisplayComplementaryCertifications());
}

function _atLeastOneWithProfessionalizingCertification(viewModels) {
  return _.some(viewModels, (viewModel) => viewModel.shouldDisplayProfessionalizingCertificationMessage());
}

function _atLeastOneWithoutProfessionalizingCertification(viewModels) {
  return _.some(viewModels, (viewModel) => !viewModel.shouldDisplayProfessionalizingCertificationMessage());
}

async function _loadTemplateByFilename(templateFileName, dirname) {
  const path = `${dirname}/files/${templateFileName}`;
  return readFile(path);
}

async function _render({ templatePdfPages, pdfDocument, viewModels, rgb, embeddedFonts, embeddedImages }) {
  for (const viewModel of viewModels) {
    const newPage = pdfDocument.addPage();

    const templatePage = await _getTemplatePage(viewModel, templatePdfPages);
    newPage.drawPage(templatePage);

    // Note: calls to setFont() are mutualized outside of the _render* methods
    // to save space. Calling setFont() n times with the same fonts creates
    // unnecessary links and big documents.
    //
    // For the same reason, don't use the `font` option of `drawText()`.
    // Size gains for 140 certifs: 5 MB -> 700 kB
    newPage.setFont(embeddedFonts.openSansBold);
    _renderScore(viewModel, newPage, embeddedFonts.openSansBold);
    _renderHeaderCandidateInformations(viewModel, newPage, rgb);

    newPage.setFont(embeddedFonts.robotoMedium);
    _renderCompetencesDetails(viewModel, newPage, rgb);

    newPage.setFont(embeddedFonts.openSansSemiBold);
    _renderMaxScore(viewModel, newPage, rgb, embeddedFonts.openSansSemiBold);

    newPage.setFont(embeddedFonts.robotoMonoRegular);
    _renderVerificationCode(viewModel, newPage, rgb);

    _renderComplementaryCertificationStickers(viewModel, newPage, embeddedImages);
  }
}

async function _getTemplatePage(viewModel, templatePdfPages) {
  if (viewModel.shouldDisplayComplementaryCertifications()) {
    if (viewModel.shouldDisplayProfessionalizingCertificationMessage()) {
      return templatePdfPages.withProfessionalizingCertificationMessageAndWithComplementaryCertifications;
    } else {
      return templatePdfPages.withoutProfessionalizingCertificationMessageAndWithComplementaryCertifications;
    }
  } else {
    if (viewModel.shouldDisplayProfessionalizingCertificationMessage()) {
      return templatePdfPages.withProfessionalizingCertificationMessageAndWithoutComplementaryCertifications;
    }
    return templatePdfPages.withoutProfessionalizingCertificationMessageAndWithoutComplementaryCertifications;
  }
}

function _renderScore(viewModel, page, font) {
  const pixScore = viewModel.pixScore;
  const scoreFontSize = 24;
  const scoreWidth = font.widthOfTextAtSize(pixScore, scoreFontSize);

  page.drawText(pixScore, {
    x: 105 - scoreWidth / 2,
    y: 675,
    size: scoreFontSize,
  });
}

function _renderMaxScore(viewModel, page, rgb, font) {
  const maxScoreFontSize = 9;

  const maxReachableScore = viewModel.maxReachableScore;
  const maxScoreWidth = font.widthOfTextAtSize(maxReachableScore, maxScoreFontSize);

  page.drawText(maxReachableScore, {
    x: 105 - maxScoreWidth / 2,
    y: 659,
    size: maxScoreFontSize,
    color: rgb(0 / 255, 45 / 255, 80 / 255),
  });
}

function _renderHeaderCandidateInformations(viewModel, page, rgb) {
  [
    [230, 707, viewModel.fullName],
    [269, 689.5, viewModel.birth],
    [257, 674, viewModel.certificationCenter],
    [208, 657.5, viewModel.certificationDate],
  ].forEach(([x, y, text]) => {
    page.drawText(text, {
      x,
      y,
      size: 9,
      color: rgb(26 / 255, 64 / 255, 109 / 255),
    });
  });
}

function _renderVerificationCode(viewModel, page, rgb) {
  page.drawText(viewModel.verificationCode, {
    x: 410,
    y: 560,
    size: 11,
    color: rgb(37 / 255, 56 / 255, 88 / 255),
  });
}

function _renderComplementaryCertificationStickers(viewModel, page, embeddedImages) {
  let yCoordinate = 395;
  viewModel.stickers.forEach(({ url, messageParts }) => {
    const pdfImage = embeddedImages[url];
    page.drawPage(pdfImage, {
      x: 400,
      y: yCoordinate,
      width: 80,
      height: 90,
    });

    if (messageParts) {
      yCoordinate -= 10;
      messageParts.forEach((text) => {
        page.drawText(text, {
          x: 350,
          y: yCoordinate,
          size: 7,
          color: rgb(37 / 255, 56 / 255, 88 / 255),
        });
        yCoordinate -= 10;
      });
    }
    yCoordinate -= 90;
  });
}

function _renderCompetencesDetails(viewModel, page, rgb) {
  const levelName = {
    '-1': '',
    '0': '',
    '1': 'Fondamental',
    '2': 'Fondamental',
    '3': 'Approfondi',
    '4': 'Approfondi',
    '5': 'Avancé',
    '6': 'Avancé',
    '7': 'Expert',
    '8': 'Expert',
  }
  const competencesLevelCoordinates = {
    "P-ART": 427,
    "P-CONS": 403,
    "P-GRAM": 379,
    "P-MOD": 355,
    "D-MCONJ": 167,
    "D-PDV": 143,
    "D-SSENT": 119,
    "M-CHOI": 557,
    "M-COMP": 533,
    "M-DEV": 509,
    "M-ORTHO": 485,
    "T-ENCH": 297,
    "T-ORG": 273,
    "T-REP": 249
  };
  viewModel.competenceDetailViewModels.forEach((competenceDetailViewModel, ic) => {
    if (competencesLevelCoordinates[competenceDetailViewModel.id]) {
      const y = competencesLevelCoordinates[competenceDetailViewModel.id];
      if (competenceDetailViewModel.shouldBeDisplayed()) {
        let x = 0;
        if(competenceDetailViewModel.level == '1' || competenceDetailViewModel.level == '2') {
          x = 255;
        } else if(competenceDetailViewModel.level == '3' || competenceDetailViewModel.level == '4') {
          x = 260;
        } else if(competenceDetailViewModel.level == '5' || competenceDetailViewModel.level == '6') {
          x = 270;
        } else if(competenceDetailViewModel.level == '7' || competenceDetailViewModel.level == '8') {
          x = 272.5;
        }
        page.drawText(levelName[competenceDetailViewModel.level], {
          x: x,
          y: y + 5,
          size: 9,
          color: rgb(37 / 255, 56 / 255, 88 / 255),
        });
      } else {
        page.drawRectangle({
          x: 65,
          y,
          width: 210,
          height: 18,
          color: rgb(1, 1, 1),
          opacity: 0.5,
        });
      }
    }
  });
}

async function _finalizeDocument(pdfDocument) {
  const pdfBytes = await pdfDocument.save();
  const buffer = Buffer.from(pdfBytes);
  return buffer;
}

module.exports = {
  getCertificationAttestationsPdfBuffer,
};
