const solutionServiceOrdo = require('../services/solution-service-ordo');
const Validation = require('./Validation');
const Validator = require('./Validator');

/**
 * Traduction: Vérificateur de réponse pour un ORDO
 */
class ValidatorOrdo extends Validator {

  constructor({
    // attributes
    // includes
    solution,
    // references
  } = {}) {
    super({ solution });
    // attributes
    // includes
    // references
  }

  assess(answer) {
    const result = solutionServiceOrdo.match(answer.answer.value, this.solution.value);

    return new Validation({
      result,
      resultDetails: null,
    });
  }
}

module.exports = ValidatorOrdo;
