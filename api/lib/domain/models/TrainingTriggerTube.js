class TrainingTriggerTube {
  constructor({ id, tubeId, level } = {}) {
    this.id = id;
    this.tubeId = tubeId;
    this.level = level;
  }
}

module.exports = TrainingTriggerTube;
