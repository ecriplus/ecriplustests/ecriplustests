class Correction {
  constructor({ id, solution, solutionToDisplay, hint, tutorials = [], learningMoreTutorials = [], explication } = {}) {
    this.id = id;
    this.solution = solution;
    this.solutionToDisplay = solutionToDisplay;
    this.hint = hint;
    this.tutorials = tutorials;
    this.learningMoreTutorials = learningMoreTutorials;
    this.explication = explication;
  }
}

module.exports = Correction;
