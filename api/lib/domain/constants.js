const settings = require('../config');
const _ = require('lodash');

const COMPETENCES_COUNT = 16;

module.exports = {
  PIX_COUNT_BY_LEVEL: settings.features.pixCountByLevel,
  COMPETENCES_COUNT,
  MAX_CHALLENGES_PER_SKILL_FOR_CERTIFICATION: 4,
  MAX_CHALLENGES_PER_AREA_FOR_CERTIFICATION_PLUS: 4,
  MAX_MASTERY_RATE: 1,
  MINIMUM_DELAY_IN_DAYS_FOR_RESET: settings.features.dayBeforeCompetenceResetV2,
  MINIMUM_DELAY_IN_DAYS_BEFORE_IMPROVING: settings.features.dayBeforeImproving,
  MINIMUM_DELAY_IN_DAYS_BEFORE_RETRYING: settings.features.dayBeforeRetrying,

  MINIMUM_CERTIFIABLE_COMPETENCES_FOR_CERTIFIABILITY: 5,
  MINIMUM_COMPETENCE_LEVEL_FOR_CERTIFIABILITY: 1,
  MINIMUM_REPRODUCIBILITY_RATE_TO_BE_CERTIFIED: 50,
  MINIMUM_REPRODUCIBILITY_RATE_TO_BE_TRUSTED: 80,
  UNCERTIFIED_LEVEL: -1,

  MAX_LEVEL_TO_BE_AN_EASY_TUBE: 3,
  DEFAULT_LEVEL_FOR_FIRST_CHALLENGE: 2,
  MAX_DIFF_BETWEEN_USER_LEVEL_AND_SKILL_LEVEL: 2,

  ALL_TREATMENTS: ['t1', 't2', 't3', 't4'],
  LEVENSHTEIN_DISTANCE_MAX_RATE: 0.25,

  PIX_ORIGIN: 'Pix',

  PIX_ORGA: {
    SCOPE: 'pix-orga',
    NOT_LINKED_ORGANIZATION_MSG:
      "L'accès à écri+éval est limité aux membres invités. Chaque espace est géré par un administrateur écri+éval propre à l'organisation qui l'utilise. Contactez-le pour qu'il vous y invite.",
  },

  PIX_ADMIN: {
    SCOPE: 'pix-admin',
    NOT_ALLOWED_MSG: "Vous n'avez pas les droits pour vous connecter.",
    ROLES: {
      SUPER_ADMIN: 'SUPER_ADMIN',
      SUPPORT: 'SUPPORT',
      METIER: 'METIER',
      CERTIF: 'CERTIF',
    },
  },
  PIX_CERTIF: {
    SCOPE: 'pix-certif',
    NOT_LINKED_CERTIFICATION_MSG:
      "L'accès à écri+certif est limité aux centres de certification écri+. Contactez le référent de votre centre de certification si vous pensez avoir besoin d'y accéder.",
    USER_SCO_BLOCKED_CERTIFICATION_MSG:
      'L’accès à l’espace écri+certif de votre établissement est temporairement désactivé. Il sera disponible juste avant la période officielle de certification en établissement scolaire : décembre pour les lycées, février pour les collèges.',
  },
  LOCALE: {
    ENGLISH_SPOKEN: 'en',
    FRENCH_FRANCE: 'fr-fr',
    FRENCH_SPOKEN: 'fr',
  },

  STUDENT_RECONCILIATION_ERRORS: {
    RECONCILIATION: {
      IN_OTHER_ORGANIZATION: {
        email: { shortCode: 'R11', code: 'ACCOUNT_WITH_EMAIL_ALREADY_EXIST_FOR_ANOTHER_ORGANIZATION' },
        username: { shortCode: 'R12', code: 'ACCOUNT_WITH_USERNAME_ALREADY_EXIST_FOR_ANOTHER_ORGANIZATION' },
        samlId: { shortCode: 'R13', code: 'ACCOUNT_WITH_GAR_ALREADY_EXIST_FOR_ANOTHER_ORGANIZATION' },
      },
      IN_SAME_ORGANIZATION: {
        email: { shortCode: 'R31', code: 'ACCOUNT_WITH_EMAIL_ALREADY_EXIST_FOR_THE_SAME_ORGANIZATION' },
        username: { shortCode: 'R32', code: 'ACCOUNT_WITH_USERNAME_ALREADY_EXIST_FOR_THE_SAME_ORGANIZATION' },
        samlId: { shortCode: 'R33', code: 'ACCOUNT_WITH_GAR_ALREADY_EXIST_FOR_THE_SAME_ORGANIZATION' },
        anotherStudentIsAlreadyReconciled: { shortCode: 'R70', code: 'USER_ALREADY_RECONCILED_IN_THIS_ORGANIZATION' },
      },
      ACCOUNT_BELONGING_TO_ANOTHER_USER: { shortCode: 'R90', code: 'ACCOUNT_SEEMS_TO_BELONGS_TO_ANOTHER_USER' },
    },
    LOGIN_OR_REGISTER: {
      IN_DB: {
        username: { shortCode: 'S50', code: 'ACCOUNT_WITH_USERNAME_ALREADY_EXIST_IN_DB' },
      },
      IN_SAME_ORGANIZATION: {
        email: { shortCode: 'S51', code: 'ACCOUNT_WITH_EMAIL_ALREADY_EXIST_FOR_THE_SAME_ORGANIZATION' },
        username: { shortCode: 'S52', code: 'ACCOUNT_WITH_USERNAME_ALREADY_EXIST_FOR_THE_SAME_ORGANIZATION' },
        samlId: { shortCode: 'S53', code: 'ACCOUNT_WITH_GAR_ALREADY_EXIST_FOR_THE_SAME_ORGANIZATION' },
      },
      IN_OTHER_ORGANIZATION: {
        email: { shortCode: 'S61', code: 'ACCOUNT_WITH_EMAIL_ALREADY_EXIST_FOR_ANOTHER_ORGANIZATION' },
        username: { shortCode: 'S62', code: 'ACCOUNT_WITH_USERNAME_ALREADY_EXIST_FOR_ANOTHER_ORGANIZATION' },
        samlId: { shortCode: 'S63', code: 'ACCOUNT_WITH_GAR_ALREADY_EXIST_FOR_ANOTHER_ORGANIZATION' },
      },
    },
  },

  CERTIFICATION_CENTER_TYPES: {
    SUP: 'SUP',
    SCO: 'SCO',
    PRO: 'PRO',
  },

  AREA_ORDER_LISTS: {
    id: [
      'Domaine_du_MOT',
      'Domaine_de_la_PHRASE',
      'Domaine_du_TEXTE',
      'Domaine_du_DISCOURS',
    ],
    name: [
      '03. Domaine du MOT',
      '01. Domaine de la PHRASE',
      '04. Domaine du TEXTE',
      '02. Domaine du DISCOURS',
    ],
    code: [
      '03',
      '01',
      '04',
      '02',
    ],
    color: [
      'wild-strawberry',
      'emerald',
      'butterfly-bush',
      'cerulean',
    ],
  }
};
