const _ = require('lodash');
const { betaTests } = require('../../../config');

async function fetchForCampaigns({
  assessment,
  answerRepository,
  campaignRepository,
  challengeRepository,
  skillRepository,
  knowledgeElementRepository,
  campaignParticipationRepository,
  improvementService,
}) {
  const campaignSkills = await campaignRepository.findSkillsByCampaignParticipationId({
    campaignParticipationId: assessment.campaignParticipationId,
  });
  const isRetrying = await campaignParticipationRepository.isRetrying({
    campaignParticipationId: assessment.campaignParticipationId,
  });
  const [allAnswers, knowledgeElements, [skills, challenges], [betaSkills, betaChallenges]] = await Promise.all([
    answerRepository.findByAssessment(assessment.id),
    _fetchKnowledgeElements({
      assessment,
      isRetrying,
      campaignParticipationRepository,
      knowledgeElementRepository,
      improvementService,
    }),
    _fetchSkillsAndChallenges({ campaignSkills, challengeRepository }),
    _fetchBetaSkillAndChallenges({ skillRepository, challengeRepository }),
  ]);

  return {
    allAnswers,
    lastAnswer: _.isEmpty(allAnswers) ? null : _.last(allAnswers),
    targetSkills: skills,
    challenges,
    knowledgeElements,
    betaSkills,
    betaChallenges,
  };
}

async function _fetchKnowledgeElements({
  assessment,
  isRetrying = false,
  knowledgeElementRepository,
  improvementService,
}) {
  const knowledgeElements = await knowledgeElementRepository.findUniqByUserId({ userId: assessment.userId });
  return improvementService.filterKnowledgeElementsIfImproving({ knowledgeElements, assessment, isRetrying });
}

async function _fetchSkillsAndChallenges({ campaignSkills, challengeRepository }) {
  const challenges = await challengeRepository.findOperativeBySkills(campaignSkills);
  return [campaignSkills, challenges];
}

async function _fetchBetaSkillAndChallenges({ skillRepository, challengeRepository }) {
  const betaSkill = await skillRepository.findActiveByCompetenceId(betaTests.competenceId);
  return await _fetchSkillsAndChallenges({
    campaignSkills: betaSkill,
    challengeRepository,
  });
}

async function fetchForCompetenceEvaluations({
  assessment,
  answerRepository,
  challengeRepository,
  knowledgeElementRepository,
  skillRepository,
  improvementService,
}) {
  const [allAnswers, targetSkills, challenges, knowledgeElements] = await Promise.all([
    answerRepository.findByAssessment(assessment.id),
    skillRepository.findActiveByCompetenceId(assessment.competenceId),
    challengeRepository.findValidatedByCompetenceId(assessment.competenceId),
    _fetchKnowledgeElements({ assessment, knowledgeElementRepository, improvementService }),
  ]);

  return {
    allAnswers,
    lastAnswer: _.isEmpty(allAnswers) ? null : _.last(allAnswers),
    targetSkills,
    challenges,
    knowledgeElements,
  };
}

async function fetchForFlashCampaigns({
  assessmentId,
  answerRepository,
  challengeRepository,
  flashAssessmentResultRepository,
  locale,
}) {
  const [allAnswers, challenges, { estimatedLevel } = {}] = await Promise.all([
    answerRepository.findByAssessment(assessmentId),
    challengeRepository.findActiveFlashCompatible({ locale }),
    flashAssessmentResultRepository.getLatestByAssessmentId(assessmentId),
  ]);

  return {
    allAnswers,
    challenges,
    estimatedLevel,
  };
}

async function fetchForFlashLevelEstimation({ assessment, answerRepository, challengeRepository }) {
  const allAnswers = await answerRepository.findByAssessment(assessment.id);
  const challenges = await challengeRepository.getMany(allAnswers.map(({ challengeId }) => challengeId));

  return {
    allAnswers,
    challenges,
  };
}

module.exports = {
  fetchForCampaigns,
  fetchForCompetenceEvaluations,
  fetchForFlashCampaigns,
  fetchForFlashLevelEstimation,
};
