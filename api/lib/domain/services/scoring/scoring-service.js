const config = require('../../../config');
const { PIX_COUNT_BY_LEVEL } = require('../../constants');

const _ = require('lodash');

function calculateScoringInformationForCompetence({
  competenceId,
  knowledgeElements,
  allowExcessPix = false,
  allowExcessLevel = false,
}) {
  const realTotalPixScoreForCompetence = _(knowledgeElements).sumBy('earnedPix');
  const pixScoreForCompetence = _getPixScoreForOneCompetence(
    realTotalPixScoreForCompetence,
    competenceId,
    allowExcessPix
  );
  const currentLevel = _getCompetenceLevel(realTotalPixScoreForCompetence, competenceId, allowExcessLevel);
  const pixAheadForNextLevel = _getPixScoreAheadOfNextLevel(pixScoreForCompetence);
  return {
    realTotalPixScoreForCompetence,
    pixScoreForCompetence,
    currentLevel,
    pixAheadForNextLevel,
  };
}

function getBlockedLevel(level, competenceId) {
  return Math.min(
    level,
    config.features.maxReachableLevel[competenceId || 'default'] ?? config.features.maxReachableLevel['default']
  );
}

function getBlockedPixScore(pixScore, competenceId) {
  return Math.min(
    pixScore,
    config.features.maxReachablePixByCompetence[competenceId || 'default'] ??
      config.features.maxReachablePixByCompetence['default']
  );
}
function _getPixScoreForOneCompetence(exactlyEarnedPix, competenceId, allowExcessPix = false) {
  const userEarnedPix = _.floor(exactlyEarnedPix);
  if (allowExcessPix) {
    return userEarnedPix;
  }
  return getBlockedPixScore(userEarnedPix, competenceId);
}

function _getCompetenceLevel(pixScoreForCompetence, competenceId, allowExcessLevel = false) {
  const level = _.floor(pixScoreForCompetence / PIX_COUNT_BY_LEVEL);
  if (allowExcessLevel) {
    return level;
  }
  return getBlockedLevel(level, competenceId);
}

function _getPixScoreAheadOfNextLevel(earnedPix) {
  return earnedPix % PIX_COUNT_BY_LEVEL;
}

function calculatePixScore(knowledgeElements) {
  return _(knowledgeElements)
    .groupBy('competenceId')
    .map((knowledgeElementsByCompetence, competenceId) =>
      calculateScoringInformationForCompetence({ competenceId, knowledgeElements: knowledgeElementsByCompetence })
    )
    .sumBy('pixScoreForCompetence');
}

module.exports = {
  calculateScoringInformationForCompetence,
  getBlockedLevel,
  getBlockedPixScore,
  calculatePixScore,
};
