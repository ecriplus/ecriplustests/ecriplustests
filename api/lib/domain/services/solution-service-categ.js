const _ = require('../../infrastructure/utils/lodash-utils');

const AnswerStatus = require('../models/AnswerStatus');

module.exports = {

  match(answer, solution) {
    let responseStatus = AnswerStatus.OK;
    const answerArray = answer.split(',\n,');
    const solutionArray = solution.split('\n');
    for (let indexCategory = 0; indexCategory < answerArray.length; indexCategory++) {
      if (!_.areCSVequivalent(answerArray[indexCategory], solutionArray[indexCategory])) {
        responseStatus = AnswerStatus.KO;
      }
    }
    return responseStatus;
  },
};
