const { UserNotAuthorizedToAccessEntityError } = require('../errors');
const { AREA_ORDER_LISTS } = require('../constants');

module.exports = async function getCampaignAssessmentParticipationResult({
  userId,
  campaignId,
  campaignParticipationId,
  campaignRepository,
  campaignAssessmentParticipationResultRepository,
  locale,
} = {}) {
  if (!(await campaignRepository.checkIfUserOrganizationHasAccessToCampaign(campaignId, userId))) {
    throw new UserNotAuthorizedToAccessEntityError('User does not belong to the organization that owns the campaign');
  }

  const campaignAssessmentParticipationResult = await campaignAssessmentParticipationResultRepository.getByCampaignIdAndCampaignParticipationId({
    campaignId,
    campaignParticipationId,
    locale,
  });
  if(campaignAssessmentParticipationResult.competenceResults){
    campaignAssessmentParticipationResult.competenceResults.sort((result, previousResult) => {
      return AREA_ORDER_LISTS.color.indexOf(result.areaColor) - AREA_ORDER_LISTS.color.indexOf(previousResult.areaColor);
    });
  }

  return campaignAssessmentParticipationResult;
};
