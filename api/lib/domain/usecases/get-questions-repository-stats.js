const bluebird = require('bluebird');

module.exports = async function getQuestionsRepositoryStats({ skillRepository }) {
  return bluebird.props({
    maxReachableScore: skillRepository.getMaxReachableScore(),
    maxReachableLevel: skillRepository.getMaxReachableLevel(),
  });
};
