const { NotFoundError } = require('../../errors');
const { AREA_ORDER_LISTS } = require('../../constants');

module.exports = async function getPrivateCertificate({ certificationId, userId, locale, certificateRepository }) {
  const privateCertificate = await certificateRepository.getPrivateCertificate(certificationId, { locale });
  if (privateCertificate.userId !== userId) {
    throw new NotFoundError();
  }
  if(privateCertificate.resultCompetenceTree){
    privateCertificate.resultCompetenceTree.areas.sort((card, previousCard) => {
      return AREA_ORDER_LISTS.id.indexOf(card.id) - AREA_ORDER_LISTS.id.indexOf(previousCard.id);
    });
  }

  return privateCertificate;
};
