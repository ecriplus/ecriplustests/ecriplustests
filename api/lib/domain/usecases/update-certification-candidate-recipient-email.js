module.exports = async function updateCertificationCandidateRecipientEmail({
  certificationCandidateId,
  resultRecipientEmail,
  certificationCandidateRepository
}) {
  await certificationCandidateRepository.updateRecipientEmail({
    id: certificationCandidateId,
    resultRecipientEmail,
  });
};
