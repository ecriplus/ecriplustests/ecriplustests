const { AREA_ORDER_LISTS } = require('../constants')
module.exports = async function getJuryCertification({ certificationCourseId, juryCertificationRepository }) {
  const juryCertification = await juryCertificationRepository.get(certificationCourseId)
  juryCertification.competenceMarks.sort((competence, previousCompetence) => {
    return AREA_ORDER_LISTS.code.indexOf(competence.area_code) - AREA_ORDER_LISTS.code.indexOf(previousCompetence.area_code);
  });

  return juryCertification;
};
