const Scorecard = require('../models/Scorecard');
const config = require('../../config');
const _ = require('lodash');
const { AREA_ORDER_LISTS } = require('../constants');

module.exports = async function getUserProfile({
  userId,
  competenceRepository,
  areaRepository,
  competenceEvaluationRepository,
  knowledgeElementRepository,
  locale,
}) {
  const [knowledgeElementsGroupedByCompetenceId, competences, competenceEvaluations] = await Promise.all([
    knowledgeElementRepository.findUniqByUserIdGroupedByCompetenceId({ userId }),
    competenceRepository.listPixCompetencesOnly({ locale }),
    competenceEvaluationRepository.findByUserId(userId),
  ]);
  const allAreas = await areaRepository.list({ locale });

  const scorecards = _.map(competences, (competence) => {
    const competenceId = competence.id;
    const knowledgeElementsForCompetence = knowledgeElementsGroupedByCompetenceId[competenceId];
    const competenceEvaluation = _.find(competenceEvaluations, { competenceId });
    const area = allAreas.find((area) => area.id === competence.areaId);
    return Scorecard.buildFrom({
      userId,
      knowledgeElements: knowledgeElementsForCompetence,
      competence,
      area,
      competenceEvaluation,
    });
  });
  scorecards.sort((card, previousCard) => {
    return AREA_ORDER_LISTS.id.indexOf(card.area.id) - AREA_ORDER_LISTS.id.indexOf(previousCard.area.id);
  });

  const pixScore = _.sumBy(scorecards, 'earnedPix');
  const maxReachableLevel = config.features.maxReachableLevel;
  const maxReachablePixScore = config.features.maxReachablePixScore;

  return {
    id: userId,
    pixScore,
    scorecards,
    maxReachablePixScore,
    maxReachableLevel,
  };
};
