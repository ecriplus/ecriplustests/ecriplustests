const { UserNotAuthorizedToAccessEntityError } = require('../errors');
const { AREA_ORDER_LISTS } = require('../constants');

module.exports = async function getCampaignProfile({
  userId,
  campaignId,
  campaignParticipationId,
  campaignRepository,
  campaignProfileRepository,
  locale,
}) {
  if (!(await campaignRepository.checkIfUserOrganizationHasAccessToCampaign(campaignId, userId))) {
    throw new UserNotAuthorizedToAccessEntityError('User does not belong to an organization that owns the campaign');
  }
  const campaignProfile = await campaignProfileRepository.findProfile({ campaignId, campaignParticipationId, locale });
  if(campaignProfile.placementProfile) {
    campaignProfile.placementProfile.userCompetences.sort((competence, previousCompetence) => {
      return AREA_ORDER_LISTS.id.indexOf(competence.areaId) - AREA_ORDER_LISTS.id.indexOf(previousCompetence.areaId);
    });
  }
  
  return campaignProfile;
};
