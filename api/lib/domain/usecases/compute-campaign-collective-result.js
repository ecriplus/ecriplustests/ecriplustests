const { UserNotAuthorizedToAccessEntityError } = require('../errors');
const CampaignLearningContent = require('../models/CampaignLearningContent');
const { AREA_ORDER_LISTS } = require('../constants');
module.exports = async function computeCampaignCollectiveResult({
  userId,
  campaignId,
  campaignRepository,
  campaignCollectiveResultRepository,
  learningContentRepository,
  locale,
} = {}) {
  const hasUserAccessToResult = await campaignRepository.checkIfUserOrganizationHasAccessToCampaign(campaignId, userId);

  if (!hasUserAccessToResult) {
    throw new UserNotAuthorizedToAccessEntityError('User does not have access to this campaign');
  }

  const learningContent = await learningContentRepository.findByCampaignId(campaignId, locale, true);
  const campaignLearningContent = new CampaignLearningContent(learningContent);
  const campaignCollectiveResult = await campaignCollectiveResultRepository.getCampaignCollectiveResult(campaignId, campaignLearningContent);
  if(campaignCollectiveResult.campaignCompetenceCollectiveResults){
    campaignCollectiveResult.campaignCompetenceCollectiveResults.sort((result, previousResult) => {
      return AREA_ORDER_LISTS.color.indexOf(result.areaColor) - AREA_ORDER_LISTS.color.indexOf(previousResult.areaColor);
    });
  }
  
  return campaignCollectiveResult;
};
