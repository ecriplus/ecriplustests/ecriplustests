
const INDEX_NAME = 'users_email_lower';
const TABLE_NAME = 'users'

exports.up = async knex => {
    await knex.raw(`DROP INDEX IF EXISTS "${INDEX_NAME}"`);
    await knex.raw(`CREATE UNIQUE INDEX "${INDEX_NAME}" ON "${TABLE_NAME}"(LOWER("email"))`);
    return knex.schema.alterTable(TABLE_NAME, (table) => {
        table.dropUnique('email');
    });
};

exports.down = async knex => {
    await knex.raw(`DROP INDEX IF EXISTS "${INDEX_NAME}"`);
    await knex.raw(`CREATE INDEX "${INDEX_NAME}" ON "${TABLE_NAME}"(LOWER("email"))`);
    return knex.schema.alterTable(TABLE_NAME, (table) => {
        table.unique('email');
    });
};
