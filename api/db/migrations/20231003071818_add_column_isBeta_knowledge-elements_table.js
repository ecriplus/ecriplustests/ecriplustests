const TABLE_NAME = 'knowledge-elements';
const COLUMN_NAME = 'isBeta';
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema.alterTable(TABLE_NAME, function(t){
    t.boolean(COLUMN_NAME).nullable().defaultTo(false);
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.table(TABLE_NAME, function(table){
    table.dropColumn(COLUMN_NAME);
  });
};
