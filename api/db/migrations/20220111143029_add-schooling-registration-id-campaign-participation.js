const TABLE_NAME = 'campaign-participations';
const REFERENCE_TABLE_NAME = 'schooling-registrations';
const COLUMN_NAME = 'schoolingRegistrationId';

exports.up = async function(knex) {
    await knex.schema.hasColumn(TABLE_NAME, COLUMN_NAME).then(async (exists) => {
      // exist might be 0/1 not necessarily false thougs probably with == operator 
      //it would work anyways
      if (!exists) { 
        await knex.schema.table(TABLE_NAME, (table) => {
          table.integer(COLUMN_NAME).references(`${REFERENCE_TABLE_NAME}.id`).defaultTo(null);
        });
      }
    })
  }

exports.down = async (knex) => {
  await knex.schema.table(TABLE_NAME, function (table) {
    table.dropColumn(COLUMN_NAME);
  });
};
