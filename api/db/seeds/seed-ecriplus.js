'use strict';

const DatabaseBuilder = require('../database-builder/database-builder');
const ecriplusCenterMembersip = require('./data/ecriplus/certification/certification-center-menbership-builder');
const { ecriplusOrganizationBuilder } = require('./data/ecriplus/organization');
const { ecriplusTargetProfilesBuilder } = require('./data/ecriplus/targetProfiles');
const { ecriplusUsersBuilder } = require('./data/ecriplus/users-builder');
const pixAdminRolesBuilder = require('./data/ecriplus/admin-roles-builder');
const { ecriplusCampaignsBuilder } = require('./data/ecriplus/campaign');
const { ecriplusCertificationCenterBuilder } = require('./data/ecriplus/certification/certification-centers-builder');
const { certificationCenterInvitationsBuilder } = require('./data/ecriplus/certification/center-invitations-builder');
const ecriplusCertificationUsersProfils = require('./data/ecriplus/certification/certification-users-profiles-builder');
const { ecriplusCertificationSessionBuilder } = require('./data/ecriplus/certification/certification-session-builder');
const { ecriplusCertificationCandidateBuilder } = require('./data/ecriplus/certification/certification-candidates-builder');
const { ecriplusCertificationCourseBuilder } = require('./data/ecriplus/certification/certification-courses-builder');
const ecriplusCertificationScoresBuilder = require('./data/ecriplus/certification/certification-score-builder');
const { fillCampaignSkills } = require('./data/fill-campaign-skills');
const { certificationCpfCountryBuilder } = require('./data/certification/certification-cpf-country-builder');
const { certificationCpfCityBuilder } = require('./data/certification/certification-cpf-city-builder');
const {
  addLastAssessmentResultCertificationCourse,
} = require('../../scripts/certification/fill-last-assessment-result-certification-course-table');
const { issueReportCategoriesBuilder } = require('./data/certification/issue-report-categories-builder');

exports.seed = async (knex) => {

  const databaseBuilder = new DatabaseBuilder({ knex });

  // Users
  ecriplusUsersBuilder({ databaseBuilder });
  pixAdminRolesBuilder({ databaseBuilder });

  // Organizations
  ecriplusOrganizationBuilder({ databaseBuilder });

  // Target Profiles
  ecriplusTargetProfilesBuilder({ databaseBuilder });

  // Certifications
  ecriplusCertificationCenterBuilder({ databaseBuilder });
  certificationCenterInvitationsBuilder({ databaseBuilder });
  ecriplusCenterMembersip({ databaseBuilder });
  await ecriplusCertificationUsersProfils({ databaseBuilder });
  ecriplusCertificationSessionBuilder({ databaseBuilder });
  ecriplusCertificationCandidateBuilder({ databaseBuilder });
  await ecriplusCertificationCourseBuilder({ databaseBuilder });
  ecriplusCertificationScoresBuilder({ databaseBuilder });
  certificationCpfCountryBuilder({ databaseBuilder });
  certificationCpfCityBuilder({ databaseBuilder });

  // Éléments de parcours
  ecriplusCampaignsBuilder({ databaseBuilder });

  // Issue report
  issueReportCategoriesBuilder({ databaseBuilder });

  await databaseBuilder.commit()
  await databaseBuilder.fixSequences();
  await fillCampaignSkills();
  await addLastAssessmentResultCertificationCourse();
};

