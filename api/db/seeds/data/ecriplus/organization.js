const Membership = require('../../../../lib/domain/models/Membership');
const { ADMIN_ORGA_ID } = require('./users-builder');

const ORGA_ECRIPLUS_ID = 1;

function ecriplusOrganizationBuilder({ databaseBuilder }) {
  const supOrganization = databaseBuilder.factory.buildOrganization({
    id: ORGA_ECRIPLUS_ID,
    type: 'SUP',
    name: 'Etablissement de test Ecriplus',
    logoUrl: 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\'/%3E',
    isManagingStudents: false,
    canCollectProfiles: true,
    externalId: 'EPO',
    provinceCode: null,
    email: null,
  });

  databaseBuilder.factory.buildMembership({
    userId: ADMIN_ORGA_ID,
    organizationId: supOrganization.id,
    organizationRole: Membership.roles.ADMIN,
  });
}

module.exports = {
  ecriplusOrganizationBuilder,
  ORGA_ECRIPLUS_ID,
};
