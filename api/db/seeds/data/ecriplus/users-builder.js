const { faker } = require('@faker-js/faker/locale/fr');
const _ = require('lodash');

const ADMIN_ORGA_ID = 20;
const CERTIF_ID = 30;
const EMPTY_USER_ID_START = 100;
const CERTIF_USER_ID_START = 200;
const CERTIF_SUCCESS_USER_ID_START = 300;
const CERTIF_FAILED_USER_ID_START = 400;
const SUPER_ADMIN_ID = 10;
const NB_USERS = 10;

const defaultPassword = 'ecri+';

faker.seed(1);

const CERTIF_CANDIDATE_IDS_SUCCESS = _.times(NB_USERS, () => {
  return {
    lastName: faker.person.lastName(),
    firstName: faker.person.firstName(),
    externalId: faker.helpers.fromRegExp('[0-9]{9}[A-Z]{2}'),
  };
});

const CERTIF_CANDIDATE_IDS_FAILURE = _.times(NB_USERS, () => {
  return {
    lastName: faker.person.lastName(),
    firstName: faker.person.firstName(),
    externalId: faker.helpers.fromRegExp('[0-9]{9}[A-Z]{2}'),
  };
});

function ecriplusUsersBuilder({ databaseBuilder }) {
  databaseBuilder.factory.buildUser.withRawPassword({
    id: ADMIN_ORGA_ID,
    firstName: 'Eval',
    lastName: faker.person.lastName(),
    email: 'eval@mail.com',
    rawPassword: defaultPassword,
    cgu: true,
  });

  databaseBuilder.factory.buildUser.withRawPassword({
    id: CERTIF_ID,
    firstName: 'Certif',
    lastName: faker.person.lastName(),
    email: 'certif@mail.com',
    rawPassword: defaultPassword,
    cgu: true,
  });

  databaseBuilder.factory.buildUser.withRawPassword({
    id: SUPER_ADMIN_ID,
    firstName: 'Super',
    lastName: 'Admin',
    email: 'admin@mail.com',
    rawPassword: defaultPassword,
  });

  for (let i = 0; i < NB_USERS; i++) {
    databaseBuilder.factory.buildUser.withRawPassword({
      id: EMPTY_USER_ID_START + i,
      firstName: `Nouveau ${i + 1} ` + faker.person.firstName(),
      lastName: faker.person.lastName(),
      email: `nouveau${i + 1}@mail.com`,
      rawPassword: defaultPassword,
      cgu: true,
    });
    databaseBuilder.factory.buildUser.withRawPassword({
      id: CERTIF_USER_ID_START + i,
      firstName: `Certifiable ${i + 1} ` + faker.person.firstName(),
      lastName: faker.person.lastName(),
      email: `certifiable${i + 1}@mail.com`,
      rawPassword: defaultPassword,
      cgu: true,
    });
    databaseBuilder.factory.buildUser.withRawPassword({
      id: CERTIF_SUCCESS_USER_ID_START + i,
      firstName: `Certif réussi ${i + 1} ` + CERTIF_CANDIDATE_IDS_SUCCESS[i].firstName,
      lastName: CERTIF_CANDIDATE_IDS_SUCCESS[i].lastName,
      email: `certif-ok${i + 1}@mail.com`,
      rawPassword: defaultPassword,
      cgu: true,
    });
    databaseBuilder.factory.buildUser.withRawPassword({
      id: CERTIF_FAILED_USER_ID_START + i,
      firstName: `Certif raté ${i + 1} ` + CERTIF_CANDIDATE_IDS_FAILURE[i].firstName,
      lastName: CERTIF_CANDIDATE_IDS_FAILURE[i].lastName,
      email: `certif-ko${i + 1}@mail.com`,
      rawPassword: defaultPassword,
      cgu: true,
    });
  }
}

module.exports = {
  ecriplusUsersBuilder,
  ADMIN_ORGA_ID,
  CERTIF_ID,
  EMPTY_USER_ID_START: EMPTY_USER_ID_START,
  CERTIF_USER_ID_START: CERTIF_USER_ID_START,
  CERTIF_SUCCESS_USER_ID_START: CERTIF_SUCCESS_USER_ID_START,
  CERTIF_FAILED_USER_ID_START: CERTIF_FAILED_USER_ID_START,
  CERTIF_CANDIDATE_IDS_SUCCESS,
  CERTIF_CANDIDATE_IDS_FAILURE,
  SUPER_ADMIN_ID,
  NB_USERS,
};
