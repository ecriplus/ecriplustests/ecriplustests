const { SUPER_ADMIN_ID, CERTIF_ID } = require('./users-builder');
const { ROLES } = require('../../../../lib/domain/constants').PIX_ADMIN;

module.exports = function pixAdminRolesBuilder({ databaseBuilder }) {
  databaseBuilder.factory.buildPixAdminRole({ userId: SUPER_ADMIN_ID, role: ROLES.SUPER_ADMIN });
  databaseBuilder.factory.buildPixAdminRole({ userId: CERTIF_ID, role: ROLES.CERTIF });
};
