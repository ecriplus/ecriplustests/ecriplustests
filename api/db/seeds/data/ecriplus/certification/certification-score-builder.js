const _ = require('lodash');
const {
  ASSESSMENT_SUCCESS_PUBLISHED_SESSION_ID_START: ASSESSMENT_SUCCESS_PUBLISHED_SESSION_ID_START,
  ASSESSMENT_FAILURE_PUBLISHED_SESSION_ID: ASSESSMENT_FAILURE_PUBLISHED_SESSION_ID,
  NB_USERS,
} = require('./certification-courses-builder');
const {
  CERTIFICATION_SUCCESS_ANSWERS_DATA,
  CERTIFICATION_SUCCESS_COMPETENCE_MARKS_DATA,
  CERTIFICATION_FAILURE_ANSWERS_DATA,
  CERTIFICATION_FAILURE_COMPETENCE_MARKS_DATA,
} = require('./certification-data');

function _buildAnswers({ answerData, databaseBuilder, assessmentId, createdAt }) {
  let answerDate = createdAt;
  _.each(answerData, (answerData) => {
    databaseBuilder.factory.buildAnswer({ ...answerData, value: 'Dummy value', assessmentId, createdAt: answerDate });
    answerDate = _addTenSeconds(answerDate);
  });
}

function _addTenSeconds(date) {
  const returnedDate = new Date(date);
  returnedDate.setSeconds(returnedDate.getSeconds() + 10);
  return returnedDate;
}

function _buildSuccessScore(databaseBuilder, createdAt, assessmentId) {
  _buildAnswers({ answerData: CERTIFICATION_SUCCESS_ANSWERS_DATA, databaseBuilder, assessmentId, createdAt });
  const assessmentResultId = databaseBuilder.factory.buildAssessmentResult({
    level: null,
    pixScore: 518,
    emitter: 'ECR-ALGO',
    status: 'validated',
    commentForJury: null,
    commentForCandidate: null,
    commentForOrganization: null,
    juryId: null,
    assessmentId,
    createdAt,
  }).id;
  _.each(CERTIFICATION_SUCCESS_COMPETENCE_MARKS_DATA, (competenceMarkData) => {
    databaseBuilder.factory.buildCompetenceMark({ ...competenceMarkData, assessmentResultId, createdAt });
  });
}

function _buildFailureScore(databaseBuilder, createdAt, assessmentId) {
  _buildAnswers({ answerData: CERTIFICATION_FAILURE_ANSWERS_DATA, databaseBuilder, assessmentId, createdAt });
  const assessmentResultId = databaseBuilder.factory.buildAssessmentResult({
    level: -1,
    pixScore: 0,
    emitter: 'ECR-ALGO',
    status: 'rejected',
    commentForJury: null,
    commentForCandidate: null,
    commentForOrganization: null,
    juryId: null,
    assessmentId,
    createdAt,
  }).id;
  _.each(CERTIFICATION_FAILURE_COMPETENCE_MARKS_DATA, (competenceMarkData) => {
    databaseBuilder.factory.buildCompetenceMark({ ...competenceMarkData, assessmentResultId, createdAt });
  });
}

function ecriplusCertificationScoresBuilder({ databaseBuilder }) {
  const createdAt = new Date();
  for (let i = 0; i < NB_USERS; i++) {
    let assessmentId = ASSESSMENT_SUCCESS_PUBLISHED_SESSION_ID_START + i;
    _buildSuccessScore(databaseBuilder, createdAt, assessmentId);

    assessmentId = ASSESSMENT_FAILURE_PUBLISHED_SESSION_ID + i;
    _buildFailureScore(databaseBuilder, createdAt, assessmentId);
  }
}

module.exports = ecriplusCertificationScoresBuilder;
