const _ = require('lodash');

const { PUBLISHED_SESSION_ID: PUBLISHED_SESSION_ID } = require('./certification-session-builder');
const {
  CERTIF_SUCCESS_USER_ID_START,
  CERTIF_FAILED_USER_ID_START,
  NB_USERS,
  CERTIF_CANDIDATE_IDS_SUCCESS,
  CERTIF_CANDIDATE_IDS_FAILURE,
} = require('../users-builder');

const SUCCESS_CANDIDATE_IN_SESSION_TO_FINALIZE_ID = 1;
const FAILURE_CANDIDATE_IN_SESSION_TO_FINALIZE_ID = 100;

const CANDIDATE_DATA_SUCCESS_BASE = {
  firstName: 'certif ok',
  lastName: 'user',
  birthdate: '2000-01-01',
  birthCity: 'Ici',
  resultRecipientEmail: 'destinaire-des-resulats@orga.com',
};
const CANDIDATE_DATA_FAILURE_BASE = {
  firstName: 'certif ko',
  lastName: 'user',
  birthdate: '2000-01-01',
  birthCity: 'Ici',
  resultRecipientEmail: 'destinaire-des-resulats@orga.com',
};
const CANDIDATE_SUCCESS_IN_SESSION = 0;
const CANDIDATE_FAILED_IN_SESSION = 0;

const CANDIDATE_DATA_SUCCESS = _.map(CERTIF_CANDIDATE_IDS_SUCCESS, (certifId, i) => {
  return {
    ...CANDIDATE_DATA_SUCCESS_BASE,
    userId: CERTIF_SUCCESS_USER_ID_START + i,
    lastName: certifId.lastName,
    firstName: 'certif ok ' + certifId.firstName,
    externalId: certifId.externalId,
  };
});

const CANDIDATE_DATA_FAILURE = _.map(CERTIF_CANDIDATE_IDS_FAILURE, (certifId, i) => {
  return {
    ...CANDIDATE_DATA_FAILURE_BASE,
    userId: CERTIF_FAILED_USER_ID_START + i,
    lastName: certifId.lastName,
    firstName: 'certif ko ' + certifId.firstName,
    externalId: certifId.externalId,
  };
});

function ecriplusCertificationCandidateBuilder({ databaseBuilder }) {
  let sessionId;

  for (let i = 0; i < NB_USERS; i++) {
    const candidateDataSuccessWithUser = CANDIDATE_DATA_SUCCESS[i];
    const candidateDataFailureWithUser = CANDIDATE_DATA_FAILURE[i];

    sessionId = PUBLISHED_SESSION_ID;
    databaseBuilder.factory.buildCertificationCandidate({
      id: SUCCESS_CANDIDATE_IN_SESSION_TO_FINALIZE_ID + i,
      ...candidateDataSuccessWithUser,
      sessionId,
    });

    databaseBuilder.factory.buildCertificationCandidate({
      id: FAILURE_CANDIDATE_IN_SESSION_TO_FINALIZE_ID + i,
      ...candidateDataFailureWithUser,
      sessionId,
    });
  }
}

module.exports = {
  ecriplusCertificationCandidateBuilder,
  CANDIDATE_SUCCESS_IN_SESSION,
  CANDIDATE_FAILED_IN_SESSION,
  CANDIDATE_CERTIF_SUCCESS_DATA: CANDIDATE_DATA_SUCCESS,
  CANDIDATE_CERTIF_FAILED_DATA: CANDIDATE_DATA_FAILURE,
};
