// const CompetenceEvaluation = require('../../../../../lib/domain/models/CompetenceEvaluation');
// const { CERTIFIABLE_USER_DATA } = require('./certification-data');
const {
  CERTIF_USER_ID_START,
  CERTIF_SUCCESS_USER_ID_START,
  CERTIF_FAILED_USER_ID_START,
  NB_USERS,
} = require('../users-builder');
const { makeUserPixCertifiable } = require('../../certification/tooling');

async function certificationUserProfilesBuilder({ databaseBuilder }) {

  for (let i = 0; i < NB_USERS; i++) {
    // Pour récuperer les compétenceId
    // const competenceIdSet = new Set();

    // assessmentIdForCertifUser = databaseBuilder.factory.buildAssessment({ userId: CERTIF_USER_ID_START + i, type: 'COMPETENCE_EVALUATION', state: 'completed' }).id;
    // assessmentIdForCertifSuccesUser = databaseBuilder.factory.buildAssessment({ userId: CERTIF_SUCCESS_USER_ID_START + i, type: 'COMPETENCE_EVALUATION', state: 'completed' }).id;
    // assessmentIdForCertifFailedUser = databaseBuilder.factory.buildAssessment({ userId: CERTIF_FAILED_USER_ID_START + i, type: 'COMPETENCE_EVALUATION', state: 'completed' }).id;

    // CERTIFIABLE_USER_DATA.forEach((answ) => {
    //   competenceIdSet.add(answ.competenceId);
    //   let answerId = databaseBuilder.factory.buildAnswer({ ...answ, createdAt, updatedAt, assessmentId: assessmentIdForCertifUser, value: 'Dummy value' }).id;
    //   databaseBuilder.factory.buildKnowledgeElement({ ...answ, createdAt, answerId, userId: CERTIF_USER_ID_START + i, assessmentId: assessmentIdForCertifUser });
    //   answerId = databaseBuilder.factory.buildAnswer({ ...answ, createdAt, updatedAt, assessmentId: assessmentIdForCertifSuccesUser, value: 'Dummy value' }).id;
    //   databaseBuilder.factory.buildKnowledgeElement({ ...answ, createdAt, answerId, userId: CERTIF_SUCCESS_USER_ID_START + i, assessmentId: assessmentIdForCertifSuccesUser });
    //   answerId = databaseBuilder.factory.buildAnswer({ ...answ, createdAt, updatedAt, assessmentId: assessmentIdForCertifFailedUser, value: 'Dummy value' }).id;
    //   databaseBuilder.factory.buildKnowledgeElement({ ...answ, createdAt, answerId, userId: CERTIF_FAILED_USER_ID_START + i, assessmentId: assessmentIdForCertifFailedUser });
    // });

    // await competenceIdSet.forEach((competenceId) => {
    const certifSuccessUserId = CERTIF_SUCCESS_USER_ID_START + i;
    const certifFailedUserId = CERTIF_FAILED_USER_ID_START + i;
    const certifiableUserId = CERTIF_USER_ID_START + i;

    await makeUserPixCertifiable({
      userId: certifSuccessUserId,
      databaseBuilder,
      countCertifiableCompetences: 16,
      levelOnEachCompetence: 8,
    });

    await makeUserPixCertifiable({
      userId: certifFailedUserId,
      databaseBuilder,
      countCertifiableCompetences: 16,
      levelOnEachCompetence: 8,
    });

    await makeUserPixCertifiable({
      userId: certifiableUserId,
      databaseBuilder,
      countCertifiableCompetences: 16,
      levelOnEachCompetence: 8,
    });

    // databaseBuilder.factory.buildCompetenceEvaluation({
    //   assessmentIdForCertifSuccesUser,
    //   competenceId,
    //   status: CompetenceEvaluation.statuses.STARTED,
    //   userId: certifSuccessUserId
    // });

    // databaseBuilder.factory.buildCompetenceEvaluation({
    //   assessmentIdForCertifUser,
    //   competenceId,
    //   status: CompetenceEvaluation.statuses.STARTED,
    //   userId: certifiableUserId
    // });
    // });
  }
}
module.exports = certificationUserProfilesBuilder;
