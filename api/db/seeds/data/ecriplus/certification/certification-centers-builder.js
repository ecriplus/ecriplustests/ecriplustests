const ECRIPLUS_CENTER_ID = 1;
const ECRIPLUS_CENTER_NAME = 'Centre test ecriplus';
const ECRIPLUS_CENTER_EXTERNAL_ID = '1237457B';
const PIX_DROIT_COMPLEMENTARY_CERTIFICATION_ID = 53;

function ecriplusCertificationCenterBuilder({ databaseBuilder }) {
  databaseBuilder.factory.buildCertificationCenter({
    id: ECRIPLUS_CENTER_ID,
    name: ECRIPLUS_CENTER_NAME,
    externalId: ECRIPLUS_CENTER_EXTERNAL_ID,
    type: 'SUP',
  });
}

module.exports = {
  ecriplusCertificationCenterBuilder,
  ECRIPLUS_CENTER_ID,
  ECRIPLUS_CENTER_NAME,
  ECRIPLUS_CENTER_EXTERNAL_ID,
  PIX_DROIT_COMPLEMENTARY_CERTIFICATION_ID,
};
