const _ = require('lodash');
const { CERTIFICATION_CHALLENGES_DATA } = require('./certification-data');
const { CERTIF_SUCCESS_USER_ID_START, CERTIF_FAILED_USER_ID_START, NB_USERS } = require('../users-builder');
const { PUBLISHED_SESSION_ID: PUBLISHED_SESSION_ID } = require('./certification-session-builder');
const { CANDIDATE_CERTIF_SUCCESS_DATA, CANDIDATE_CERTIF_FAILED_DATA } = require('./certification-candidates-builder');
const AssessmentResult = require('../../../../../lib/domain/models/AssessmentResult');
const {
  generateCertificateVerificationCode,
} = require('../../../../../lib/domain/services/verify-certificate-code-service');
const ASSESSMENT_SUCCESS_PUBLISHED_SESSION_ID_START = 100;
const ASSESSMENT_FAILURE_PUBLISHED_SESSION_ID = 200;
const CERTIFICATION_COURSE_SUCCESS_ID_START = 100;
const CERTIFICATION_COURSE_FAILURE_ID_START = 200;

async function _buildCertificationCourse(
  databaseBuilder,
  {
    id,
    assessmentId,
    userId,
    sessionId,
    candidateData,
    examinerComment,
    hasSeenEndTestScreen,
    isPublished,
    pixCertificationStatus,
  },
) {
  const createdAt = new Date();
  const updatedAt = new Date();
  const completedAt = new Date();
  const verificationCode = await generateCertificateVerificationCode();
  const certificationCourseId = databaseBuilder.factory.buildCertificationCourse({
    ...candidateData,
    id,
    createdAt,
    updatedAt,
    completedAt,
    isPublished,
    isV2Certification: true,
    examinerComment,
    birthplace: 'Ici',
    hasSeenEndTestScreen,
    sessionId,
    userId,
    verificationCode,
    pixCertificationStatus,
  }).id;

  databaseBuilder.factory.buildAssessment({
    id: assessmentId,
    certificationCourseId,
    type: 'CERTIFICATION',
    state: 'completed',
    userId,
    competenceId: null,
    campaignParticipationId: null,
    isImproving: false,
    createdAt,
    updatedAt,
    completedAt,
    pixCertificationStatus,
  });
  _.each(CERTIFICATION_CHALLENGES_DATA, (challenge) => {
    databaseBuilder.factory.buildCertificationChallenge({
      ...challenge,
      courseId: certificationCourseId,
      associatedSkillId: null,
      createdAt,
    });
  });
}

async function ecriplusCertificationCourseBuilder({ databaseBuilder }) {
  for (let i = 0; i < NB_USERS; i++) {
    const certificationInformations = [
      {
        id: CERTIFICATION_COURSE_SUCCESS_ID_START + i,
        userId: CERTIF_SUCCESS_USER_ID_START + i,
        sessionId: PUBLISHED_SESSION_ID,
        assessmentId: ASSESSMENT_SUCCESS_PUBLISHED_SESSION_ID_START + i,
        candidateData: CANDIDATE_CERTIF_SUCCESS_DATA[i],
        examinerComment: null,
        hasSeenEndTestScreen: false,
        isPublished: true,
        verificationCode: null,
        pixCertificationStatus: AssessmentResult.status.VALIDATED,
      },
      {
        id: CERTIFICATION_COURSE_FAILURE_ID_START + i,
        userId: CERTIF_FAILED_USER_ID_START + i,
        sessionId: PUBLISHED_SESSION_ID,
        assessmentId: ASSESSMENT_FAILURE_PUBLISHED_SESSION_ID + i,
        candidateData: CANDIDATE_CERTIF_FAILED_DATA[i],
        examinerComment: null,
        hasSeenEndTestScreen: false,
        isPublished: true,
        verificationCode: null,
        pixCertificationStatus: AssessmentResult.status.VALIDATED,
      },
    ];

    for (const certificationInformation of certificationInformations) {
      await _buildCertificationCourse(databaseBuilder, certificationInformation);
    }
  }
}

module.exports = {
  ecriplusCertificationCourseBuilder,
  ASSESSMENT_SUCCESS_PUBLISHED_SESSION_ID_START: ASSESSMENT_SUCCESS_PUBLISHED_SESSION_ID_START,
  ASSESSMENT_FAILURE_PUBLISHED_SESSION_ID: ASSESSMENT_FAILURE_PUBLISHED_SESSION_ID,
  NB_USERS,
};
