//Permet aux utilisateurs de se connecter à certif

const { ADMIN_ORGA_ID, CERTIF_ID } = require('../users-builder');

const { ECRIPLUS_CENTER_ID } = require('./certification-centers-builder');

module.exports = function ecriplusCenterMembersip({ databaseBuilder }) {
  databaseBuilder.factory.buildCertificationCenterMembership({
    userId: ADMIN_ORGA_ID,
    certificationCenterId: ECRIPLUS_CENTER_ID,
  });
  databaseBuilder.factory.buildCertificationCenterMembership({
    userId: CERTIF_ID,
    certificationCenterId: ECRIPLUS_CENTER_ID,
  });
};
