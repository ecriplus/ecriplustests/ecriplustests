const { ECRIPLUS_CENTER_NAME, ECRIPLUS_CENTER_ID } = require('./certification-centers-builder');
const PUBLISHED_SESSION_ID = 1;
const STARTED_SESSION_ID = 2;

function ecriplusCertificationSessionBuilder({ databaseBuilder }) {
  const certificationCenter = ECRIPLUS_CENTER_NAME;
  const certificationCenterId = ECRIPLUS_CENTER_ID;
  const address = 'Rue du test';
  const room = 'Salle de test';
  const examiner = 'Jean Graton';
  const date = new Date(new Date().setDate(new Date().getDate() - 60));
  const time = '15:00';

  databaseBuilder.factory.buildSession({
    id: PUBLISHED_SESSION_ID,
    certificationCenter,
    certificationCenterId,
    address,
    room,
    examiner,
    date,
    time,
    description: 'Session publiée.',
    accessCode: 'ECRI01',
    examinerGlobalComment: null,
    finalizedAt: new Date(new Date().setDate(new Date().getDate() - 30)),
    publishedAt: new Date(new Date().setDate(new Date().getDate() - 27)),
  });

  databaseBuilder.factory.buildFinalizedSession({
    sessionId: PUBLISHED_SESSION_ID,
    certificationCenterName: certificationCenter,
    isPublishable: true,
    date,
    time,
    finalizedAt: new Date(new Date().setDate(new Date().getDate() - 30)),
    publishedAt: new Date(new Date().setDate(new Date().getDate() - 27)),
  });

  databaseBuilder.factory.buildSession({
    id: STARTED_SESSION_ID,
    certificationCenter,
    certificationCenterId,
    address,
    room,
    examiner: `${examiner}-started`,
    date,
    time,
    description: 'Session pas commencée avec quelques candidats inscrits non liés.',
    accessCode: 'ECRI02',
    examinerGlobalComment: null,
  });
}

module.exports = {
  ecriplusCertificationSessionBuilder,
  PUBLISHED_SESSION_ID: PUBLISHED_SESSION_ID,
  STARTED_SESSION_ID: STARTED_SESSION_ID,
};
