const { LEXICALS_ID, ORTHO_ID, HIGHSCHOOL_ID, REDAC_TRANS_ID } = require('./targetProfiles');
const { ADMIN_ORGA_ID } = require('./users-builder');
const { ORGA_ECRIPLUS_ID } = require('./organization');

const LEXICAL_CAMPAIGN_ID = 1;
const ORTHO_CAMPAIGN_ID = 2;
const HIGHSCHOOL_CAMPAIGN_ID = 3;
const REDAC_TRANS_CAMPAIGN_ID = 4;

function ecriplusCampaignsBuilder({ databaseBuilder }) {
  databaseBuilder.factory.buildCampaign({
    id: LEXICAL_CAMPAIGN_ID,
    name: 'Compétences lexicales',
    code: 'EPTPFE001',
    type: 'ASSESSMENT',
    createdAt: new Date(),
    organizationId: ORGA_ECRIPLUS_ID,
    creatorId: ADMIN_ORGA_ID,
    ownerId: ADMIN_ORGA_ID,
    targetProfileId: LEXICALS_ID,
    idPixLabel: 'Numéro Étudiant',
    title: 'Compétences lexicales',
    customLandingPageText: 'Bienvenue sur la page d\'accueil de la Campagne compétences lexicales.',
  });

  databaseBuilder.factory.buildCampaign({
    id: ORTHO_CAMPAIGN_ID,
    name: 'Compétences orthographiques',
    code: 'EPTPFE002',
    type: 'ASSESSMENT',
    createdAt: new Date(),
    organizationId: ORGA_ECRIPLUS_ID,
    creatorId: ADMIN_ORGA_ID,
    ownerId: ADMIN_ORGA_ID,
    targetProfileId: ORTHO_ID,
    idPixLabel: 'Numéro Étudiant',
    title: 'Compétences orthographiques',
    customLandingPageText: 'Bienvenue sur la page d\'accueil de la Campagne compétences orthographiques.',
  });

  databaseBuilder.factory.buildCampaign({
    id: HIGHSCHOOL_CAMPAIGN_ID,
    name: 'Acquis fin de lycée',
    code: 'EPTPFE003',
    type: 'ASSESSMENT',
    createdAt: new Date(),
    organizationId: ORGA_ECRIPLUS_ID,
    creatorId: ADMIN_ORGA_ID,
    ownerId: ADMIN_ORGA_ID,
    targetProfileId: HIGHSCHOOL_ID,
    idPixLabel: 'Numéro Étudiant',
    title: 'Acquis fin de lycée',
    customLandingPageText: 'Bienvenue sur la page d\'accueil de la Campagne acquis fin de lycée.',
  });

  databaseBuilder.factory.buildCampaign({
    id: REDAC_TRANS_CAMPAIGN_ID,
    name: 'Compétences rédactionnelles transversales',
    code: 'EPTPFE004',
    type: 'ASSESSMENT',
    createdAt: new Date(),
    organizationId: ORGA_ECRIPLUS_ID,
    creatorId: ADMIN_ORGA_ID,
    ownerId: ADMIN_ORGA_ID,
    targetProfileId: REDAC_TRANS_ID,
    idPixLabel: 'Numéro Étudiant',
    title: 'Compétences rédactionnelles transversales',
    customLandingPageText: 'Bienvenue sur la page d\'accueil de la Campagne compétences rédactionnelles transversales.',
  });
}

module.exports = {
  ecriplusCampaignsBuilder,
  LEXICAL_CAMPAIGN_ID,
  ORTHO_CAMPAIGN_ID,
  HIGHSCHOOL_CAMPAIGN_ID,
};
