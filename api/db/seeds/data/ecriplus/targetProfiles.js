const { ORGA_ECRIPLUS_ID } = require('./organization');

const LEXICALS_ID = 1;
const ORTHO_ID = 2;
const HIGHSCHOOL_ID = 3;
const REDAC_TRANS_ID = 4;
const L1_ID = 5;
const L3_ID = 6;
const MEEF1_ID = 7;
const MEEF2_ID = 8;

function ecriplusTargetProfilesBuilder({ databaseBuilder }) {
  databaseBuilder.factory.buildTargetProfile({
    id: LEXICALS_ID,
    name: 'Compétences lexicales',
    isPublic: true,
    ownerOrganizationId: ORGA_ECRIPLUS_ID,
  });

  databaseBuilder.factory.buildTargetProfile({
    id: ORTHO_ID,
    name: 'Compétences orthographiques',
    isPublic: true,
    ownerOrganizationId: ORGA_ECRIPLUS_ID,
  });

  databaseBuilder.factory.buildTargetProfile({
    id: HIGHSCHOOL_ID,
    name: 'Acquis fin de lycée',
    isPublic: true,
    ownerOrganizationId: ORGA_ECRIPLUS_ID,
  });

  databaseBuilder.factory.buildTargetProfile({
    id: REDAC_TRANS_ID,
    name: 'Compétences rédactionnelles transversales',
    isPublic: true,
    ownerOrganizationId: ORGA_ECRIPLUS_ID,
  });

  databaseBuilder.factory.buildTargetProfile({
    id: L1_ID,
    name: 'Compétences L1',
    isPublic: true,
    ownerOrganizationId: ORGA_ECRIPLUS_ID,
  });

  databaseBuilder.factory.buildTargetProfile({
    id: L3_ID,
    name: 'Compétences L3',
    isPublic: true,
    ownerOrganizationId: ORGA_ECRIPLUS_ID,
  });

  databaseBuilder.factory.buildTargetProfile({
    id: MEEF1_ID,
    name: 'Acquis de base en début de licence MEEF',
    isPublic: true,
    ownerOrganizationId: ORGA_ECRIPLUS_ID,
  });

  databaseBuilder.factory.buildTargetProfile({
    id: MEEF2_ID,
    name: 'Acquis de fin de licence MEEF',
    isPublic: true,
    ownerOrganizationId: ORGA_ECRIPLUS_ID,
  });

}

module.exports = {
  ecriplusTargetProfilesBuilder,
  LEXICALS_ID,
  ORTHO_ID,
  HIGHSCHOOL_ID,
  REDAC_TRANS_ID,
  L1_ID,
  L3_ID,
};
