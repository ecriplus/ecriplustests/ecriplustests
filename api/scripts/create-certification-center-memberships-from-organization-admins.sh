#!/usr/bin/env sh

prog_name=${0##*/}
version=1.0
version_text="Create certification center membership for organization admins v$version"
options="h V"
help_text="Usage: $prog_name [-hV] <userId> <externalId> | <csvFilepath>

Create certification center membership for organization admins.
Wrap the call to create-certification-center-memberships-from-organization-admins.
This script can be called either with 1 parameters (a path to a csv file) or
with 2 parameters (user id and external id).

        -h            Display this help text and exit
        -V            Display version information and exit
        <userId>      The userId (required)
        <externalId>  The externalId (required)
        <csvFilepath> Path to a csv file (required)"

echo_csv() {
  printf "id;externalId\n$userId;$externalId\n"
}

main() {

  script=$(readlink -f "$0")
  # Absolute path this script is in, thus /home/user/bin
  scriptPath=$(dirname $(dirname "$script"))
  cd $scriptPath

  set_defaults
  parse_options "$@"
  shift $((OPTIND-1))
  # If we want to use `getopts` again, this has to be set to 1.
  OPTIND=1

  # shellcheck disable=2154
  {
    $option_h && usage
    $option_V && version
  }

  if [ "$#" -eq 1 ]; then
    csvFile=$1
    [ ! -f "$csvFile" ] && error 2 "CSV file must exists."
    [ "$(echo "$csvFile" | awk -F . '{print $NF}')" != "csv" ] && error 3 "CSV filename extension must be csv."
  elif [ "$#" -eq 2 ]; then
    userId=$1
    externalId=$2

    tmpFolder=`mktemp -d -t create-certification-center-memberships-from-organization-admins_XXXXXXXXXX` || exit 1

    csvFile="$tmpFolder/import.csv"

    echo_csv > "$csvFile"
  else
    usage 1
  fi

  node scripts/create-certification-center-memberships-from-organization-admins.js "$csvFile"

  [ -t 0 ] ||
    info "stdin is not a terminal"
}

##########################################################################

# shellcheck disable=2034,2046
set_defaults() {
  set -e
  trap 'clean_exit' EXIT TERM
  trap 'clean_exit HUP' HUP
  trap 'clean_exit INT' INT
  IFS=' '
  set -- $(printf '\n \r \t \033')
  nl=$1 cr=$2 tab=$3 esc=$4
  IFS=\ $tab
}

# For a given optstring, this function sets the variables
# "option_<optchar>" to true/false and param_<optchar> to its parameter.
parse_options() {
  for _opt in $options; do
    # The POSIX spec does not say anything about spaces in the
    # optstring, so lets get rid of them.
    _optstring=$_optstring$_opt
    eval "option_${_opt%:}=false"
  done

  while getopts ":$_optstring" _opt; do
    case $_opt in
      :) usage "option '$OPTARG' requires a parameter" ;;
      \?) usage "unrecognized option '$OPTARG'" ;;
      *)
        eval "option_$_opt=true"
        [ -n "$OPTARG" ] &&
          eval "param_$_opt=\$OPTARG"
      ;;
    esac
  done
  unset _opt _optstring OPTARG
}

info()    { printf %s\\n "$*" >&2; }
version() { printf %s\\n "$version_text"; exit; }

error() {
  _error=${1:-1}
  shift
  printf '%s: Error: %s\n' "$prog_name" "$*" >&2
  exit "$_error"
}

usage() {
  [ $# -ne 0 ] && {
    exec >&2
    printf '%s: %s\n\n' "$prog_name" "$*"
  }
  printf %s\\n "$help_text"
  exit ${1:+1}
}

clean_exit() {
  _exit_status=$?
  trap - EXIT

  [ ! -z "$tmpFolder" ] && [ -d "$tmpFolder" ] && rm -r "$tmpFolder"
  info "exiting"

  [ $# -ne 0 ] && {
    trap - "$1"
    kill -s "$1" -$$
  }
  exit "$_exit_status"
}

main "$@"
