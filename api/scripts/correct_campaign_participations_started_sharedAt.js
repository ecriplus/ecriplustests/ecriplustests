#! /usr/bin/env node
/* eslint no-console: ["off"] */
const { knex } = require('../db/knex-database-connection');


async function main() {

  await knex('campaign-participations')
    .update('status', 'SHARED')
    .whereNotNull('sharedAt')
    .whereNot('status', 'SHARED');

}


/*=================== tests =============================*/

if (require.main === module) {
  main()
    .then(() => { knex.destroy() })
    .catch((e) => {
      console.error('Unexpected error.', e)
      knex.destroy()
    })
}
