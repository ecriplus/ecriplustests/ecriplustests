#!/usr/bin/env node

require('dotenv').config();

const { knex, disconnect } = require('../../db/knex-database-connection');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');

const _ = require('lodash');
const { NotFoundError } = require('../../lib/domain/errors');

const fs = require('fs');

(async () => {
  try {
    await yargs(hideBin(process.argv))
      .command(
        'export <userId>',
        'Export user learning profile',
        (yargs) => {
          yargs.positional('userId', {
            describe: 'The user db ID',
            type: 'number',
          });
        },
        async (argv) => {
          await _export(argv);
        }
      )
      .command(
        'import <userId>',
        'Import user learning profile, in dev mode only',
        (yargs) => {
          yargs.positional('userId', {
            describe: 'The user db ID',
            type: 'number',
          });
        },
        async (argv) => {
          await _import(argv);
        }
      )
      .help('h')
      .alias('h', 'help')
      .demandCommand()
      .strict()
      .parse();
  } finally {
    await disconnect();
  }
})();

async function _export({ userId }) {
  // get assesments
  const knowledgeElements = await _getKnowledgeElements({ userId });
  const assessments = await _getUserAssessments({ userId });
  const answers = await _getAnswers(_.map(knowledgeElements, 'answerId'));
  const assessmentResults = await _getAssessmentResults({ assessmentIds: _.map(assessments, 'id') });
  const competenceMarks = await _getCompetenceMarks({ assessmentResultIds: _.map(assessmentResults, 'id') });
  const competenceEvaluations = await _getCompetenceEvaluations({ userId });

  process.stdout.write(
    JSON.stringify({
      knowledgeElements,
      assessments,
      answers,
      assessmentResults,
      competenceMarks,
      competenceEvaluations,
    })
  );
}

async function _getKnowledgeElements({ userId }) {
  return await knex('knowledge-elements').where('knowledge-elements.userId', userId);
}

async function _getAnswers(answerIds) {
  return await knex('answers').whereIn('id', answerIds).orderBy('createdAt');
}

async function _getUserAssessments({ userId }) {
  const assessments = await knex('assessments').where('assessments.userId', userId);
  if (!assessments) {
    throw new NotFoundError(`Assessment not found for userId ${userId}`);
  }

  return assessments;
}

async function _getAssessmentResults({ assessmentIds }) {
  const assessmentResults = await knex('assessment-results').whereIn('assessment-results.assessmentId', assessmentIds);
  if (!assessmentResults) {
    throw new NotFoundError(`AssessmentResults not found for assessmentIds ${assessmentIds}`);
  }

  return assessmentResults;
}

async function _getCompetenceMarks({ assessmentResultIds }) {
  const compMarks = await knex('competence-marks').whereIn('assessmentResultId', assessmentResultIds);
  return compMarks;
}

async function _getCompetenceEvaluations({ userId }) {
  const compEval = await knex('competence-evaluations').where({ userId });

  return compEval;
}

async function _import({ userId }) {
  if (process.env.NODE_ENV != 'development') {
    throw new Error('Can only run in dev');
  }
  // eslint-disable-next-line no-sync
  const rawData = fs.readFileSync(0, 'utf-8');
  const data = JSON.parse(rawData);

  await knex.transaction(async (trx) => {
    const assessmentIdMap = await _.reduce(
      data.assessments,
      async (acc, assessment) => {
        const params = _(assessment)
          .omit('id', 'certificationCourseId', 'courseId', 'campaignParticipationId')
          .assign({ userId })
          .value();
        const newIds = await trx('assessments').insert(params, ['id']);
        return _.assign(acc, { [assessment.id]: newIds[0].id });
      },
      {}
    );

    const answersIdMap = await _.reduce(
      data.answers,
      async (acc, answer) => {
        const answerParams = _(answer)
          .omit('id')
          .assign({ assessmentId: assessmentIdMap[answer.assessmentId] })
          .value();
        const newIds = await trx('answers').insert(answerParams, ['id']);
        return _.assign(acc, { [answer.id]: newIds[0].id });
      },
      {}
    );

    const keParams = _.map(data.knowledgeElements, (ke) =>
      _(ke)
        .omit('id')
        .assign({ userId, assessmentId: assessmentIdMap[ke.assessmentId], answerId: answersIdMap[ke.answerId] })
        .value()
    );
    await trx('knowledge-elements').insert(keParams);

    // insert assesmentResults
    const assessmentResultParams = _.map(data.assessmentResults, (ar) =>
      _(ar).omit('id', 'juryId').assign({ assessmentId: assessmentIdMap[ar.assessmentId] }).value()
    );
    const assessmentResultIdMap = _.zipObject(
      _.map(data.assessmentResults, 'id'),
      _.map(await trx('assessment-results').insert(assessmentResultParams, ['id']), 'id')
    );

    // insert competenceMarks
    const competenceMarkParams = _.map(data.compMarks, (cm) =>
      _(cm).omit('id').assign({ assessmentResultId: assessmentResultIdMap[cm.assessmentResultId] }).value()
    );
    await trx('competence-marks').insert(competenceMarkParams);

    // insert competenceEvaluations
    const competenceEvaluationParams = _.map(data.competenceEvaluations, (ce) =>
      _(ce).omit('id').assign({ userId, assessmentId: assessmentIdMap[ce.assessmentId] }).value()
    );
    await trx('competence-evaluations').insert(competenceEvaluationParams);
  });
}
