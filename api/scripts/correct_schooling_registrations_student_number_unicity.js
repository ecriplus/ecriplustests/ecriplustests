#! /usr/bin/env node
/* eslint no-console: ["off"] */
const { knex } = require('../db/knex-database-connection');


async function main() {
  const multipleRegistrations = await knex('schooling-registrations')
    .select('organizationId', 'studentNumber')
    .count( '*', { as: 'c' })
    .groupBy('organizationId', 'studentNumber')
    .whereNotNull('studentNumber')
    .orderBy('c', 'desc')
    .having(knex.raw('count(*) > 1'));

  const trx = await knex.transaction();
  try {
    await Promise.all(multipleRegistrations.map(async (mr) => {
      console.log(mr);
      const schoolingRegistrations = await trx('schooling-registrations')
        .where({organizationId: mr.organizationId, studentNumber: mr.studentNumber});
      await Promise.all(schoolingRegistrations.map((sr, i) => {
        if (i > 0) {
          return trx('schooling-registrations')
            .where('id', sr.id)
            .update('studentNumber', `${sr.studentNumber} __${(i+1).toLocaleString(undefined, {minimumIntegerDigits: 2})}__`);
        }
        return Promise.resolve();
      }));
    }));
    await trx.commit();
  } catch {
    await trx.rollback();
  }

  
  // return await knex.transaction((trx) => {
  //   return resList.map(async (res) => {
  //     console.log(res)
  //     const schoolingRegistrations = await trx('schooling-registrations')
  //       .where({organizationId: res.organizationId, studentNumber: res.studentNumber});
  //     return schoolingRegistrations.map((sr, i) => {
  //       console.log(sr, i);
  //     })
  //   });  
  // });
}


/*=================== tests =============================*/

if (require.main === module) {
  main()
    .then(() => { knex.destroy() })
    .catch((e) => {
      console.error('Unexpected error.', e)
      knex.destroy()
    })
}
