const { knex, disconnect } = require('../db/knex-database-connection');
const _ = require('lodash');
const { parseAsync } = require('json2csv');
const fs = require('node:fs');

function usage() {
  console.log(`Usage: node extract-beta-tests.js destinationfolder startdate enddate [--beta] [--counts] [--answers]`);
  console.log(`  destinationfolder  - Folder where the files are written (Required)`);
  console.log(`  startdate          - Format yyyy-mm-dd (Required)`);
  console.log(`  enddate            - Format yyyy-mm-dd (Required)`);
  console.log(`  --beta             - Use only betatests (Optional)`);
  console.log(`  --counts           - Get counts for each question (Optional)`);
  console.log(`  --answers          - Get answer details for each question (Optional)`);
}

async function _extractBetatestsPaginated(startDate, endDate, onlyBeta, limit, offset) {
  const isBeta = onlyBeta ? 'AND "isBeta" = True' : '';
  console.log(offset + " lines processed");
  const result = await knex.raw(
    `
    SELECT assessments."userId", assessments.id as assessmentId, "challengeId", value, result, answers."updatedAt", "resultDetails", organizations.name as organization, "isBeta"
    FROM answers
    JOIN assessments on "assessmentId" = assessments.id
    JOIN "campaign-participations" on "campaignParticipationId" = "campaign-participations".id
    JOIN "organization-learners" on "organizationLearnerId" = "organization-learners".id
    JOIN organizations on "organizationId" = organizations.id
    WHERE answers."updatedAt" BETWEEN ? AND ?
    ${isBeta}
    LIMIT ? OFFSET ?;
  `, [startDate, endDate, limit, offset]
  );
  return result.rows;
}

async function _extractBetatests(startDate, endDate, onlyBeta) {
  let offset = 0;
  const limit = 500000;
  let allRows = [];
  let rows;

  do {
    rows = await _extractBetatestsPaginated(startDate, endDate, onlyBeta, limit, offset);
    allRows = allRows.concat(rows);
    offset += limit;
  } while (rows.length === limit);

  return allRows;
}

const isLaunchedFromCommandLine = require.main === module;

function _anonymizeUserId(data) {
    let currentId = 1;
    const userIdMap = {};

    return data.map(item => {
      if (!userIdMap[item.userId]) {
        userIdMap[item.userId] = currentId++;
      }

      return {
        ...item,
        userId: userIdMap[item.userId]
      };
    });
}

async function _processInChunks(data, destination) {
  const stream = fs.createWriteStream(destination + "/question_answers.csv", { flags: 'a' });
  const chunkSize = 5000;
  let result = [];

  for (let i = 0; i < data.length; i += chunkSize) {
    const chunk = data.slice(i, i + chunkSize);
    const chunkResult = await parseAsync(chunk);
    stream.write(chunkResult);
  }
  stream.end();
}

async function main(destination, startDate, endDate, onlyBeta, isCounts, isAnswers) {
  console.log("Query started");
  const betatests = await _extractBetatests(startDate, endDate, onlyBeta);
  console.log("Query done");
  const anonymisedBetatests = _anonymizeUserId(betatests);
  const betatestsAnswersCount = betatests.reduce((acc, obj) => {
    if (acc[obj.challengeId]) {
      acc[obj.challengeId]++;
    } else {
      acc[obj.challengeId] = 1;
    }
    return acc
  }, {});

  if (isCounts) {
    console.log("writing counts file...");
    fs.writeFile(destination + "/question_counts.json", JSON.stringify(betatestsAnswersCount), err => {
      if (err) {
        console.error(err);
      } else {
        console.log("file written successfully");
      }
    });
  }

  if (isAnswers) {
    console.log("writing answers file...");
    try {
      await _processInChunks(betatests, destination);
      console.log("file written successfully");
    } catch (error) {
      throw error;
    }
  }

}

(async () => {
  if (isLaunchedFromCommandLine) {
    if (process.argv.length < 5) {
      console.error('Error: Wrong number of arguments.');
      usage();
      process.exit(1);
    }

    const destination = process.argv[2];
    const startDate = process.argv[3];
    const endDate = process.argv[4];
    let onlyBeta = false;
    let isCounts = false;
    let isAnswers = false;

    const args = process.argv.slice(5);
    if (args.includes('--beta')) onlyBeta = true;
    if (args.includes('--counts')) isCounts = true;
    if (args.includes('--answers')) isAnswers = true;
    if (!(isCounts || isAnswers)) isCounts = true; // Si aucune option renseignée, choisir counts par défaut

    try {
      await main(destination, startDate, endDate, onlyBeta, isCounts, isAnswers);
    } catch (error) {
      console.error(error);
      process.exitCode = 1;
    } finally {
      await disconnect();
    }
  }
})();
