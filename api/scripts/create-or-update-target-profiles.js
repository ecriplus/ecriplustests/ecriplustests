#! /usr/bin/env node
/* eslint no-console: ["off"] */

'use strict';
require('dotenv').config();

const { promises: fs } = require('fs');
const _ = require('lodash');
const Bluebird = require('bluebird');
const Bookshelf = require('../lib/infrastructure/bookshelf');
const TargetProfileBookshelf = require('../lib/infrastructure/orm-models/TargetProfile');
const { DomainError } = require('../lib/domain/errors');

const targetProfileTubemodelName = 'TargetProfileTube';
const TargetProfileTubeBookshelf = Bookshelf.model(
  targetProfileTubemodelName,
  {
    tableName: 'target-profile_tubes',

    targetProfile() {
      return this.belongsTo('TargetProfile', 'targetProfileId');
    },
  },
  {
    modelName: targetProfileTubemodelName,
  }
)

function usage() {
  console.log('Usage: ', 'create-or-update-target-profiles <path_to_json>');
}

async function fileExists(filepath) {
  try {
    await fs.access(filepath);
    return true;
  } catch (_) {
    return false;
  }
}

async function writeTargetProfile(targetProfileDef, trx) {
  const doUpdate = 'id' in targetProfileDef;
  const targetProfileParams = _.omit(targetProfileDef, ['tubes']);
  const writingMethod = doUpdate ? 'update' : 'insert';
  let targetProfile;

  try {
    targetProfile = await new TargetProfileBookshelf(targetProfileParams).save(null, { transacting: trx, method: writingMethod });
    if (!('tubes' in targetProfileDef) || !Array.isArray(targetProfileDef.tubes) || !targetProfileDef.tubes.length) {
      return targetProfile;
    }
  } catch (error) {
    throw new DomainError(`Error writing target profile (${writingMethod})`, 'ERR_INVALID_ARG_VALUE', { params: targetProfileParams, error: error });
  }

  //get list of tubes to delete
  let tubesToDelete = [];
  let tubesToAdd = targetProfileDef.tubes;
  if (doUpdate) {
    const existingTubesRes = await TargetProfileTubeBookshelf.where({ targetProfileId: targetProfileDef.id }).fetchAll();
    const existingTubes = existingTubesRes.pluck(['tubeId']);
    tubesToDelete = _.difference(existingTubes, targetProfileDef.tubes);
    tubesToAdd = _.difference(targetProfileDef.tubes, existingTubes);
  }

  const operationsPromises = [
    Bluebird.map(tubesToAdd, (tube) => {
      return new TargetProfileTubeBookshelf({ targetProfileId: targetProfile.id, tubeId: tube.id, level: tube.level }).save(null, { transacting: trx });
    }),
  ];

  if (tubesToDelete.length) {
    operationsPromises.push(TargetProfileTubeBookshelf.query((qb) => qb.where({ targetProfileId: targetProfile.id }).whereIn('tubeId', tubesToDelete)).destroy({ transacting: trx, require: false }));
  }

  return Bluebird.all(operationsPromises);
}

async function main() {

  if (process.argv.length != 3) {
    console.error('Wrong number of arguments.');
    usage();
    Bookshelf.knex.destroy();
    process.exit(1);
  }

  const jsonPath = process.argv[2];

  // check that json exists
  if (! await fileExists(jsonPath)) {
    console.error(`JSON file ${jsonPath} does not exist.`);
    usage();
    Bookshelf.knex.destroy();
    process.exit(2);
  }

  let targetProfilesDefList;

  try {
    const data = await fs.readFile(jsonPath);
    targetProfilesDefList = JSON.parse(data);
  } catch (err) {
    console.error(`Can not parse JSON file ${jsonPath}`, err);
    usage();
    Bookshelf.knex.destroy();
    process.exit(3);
  }

  console.log('Target Profile definitions', targetProfilesDefList);
  if (!Array.isArray(targetProfilesDefList)) {
    console.error('Target Profile definitions is not an array');
    usage();
    Bookshelf.knex.destroy();
    process.exit(4);
  }

  try {
    await Bookshelf.transaction(async (trx) => {

      try {
        await Bluebird.map(targetProfilesDefList, (targetProfilesDef) => {
          return writeTargetProfile(targetProfilesDef, trx);
        });
      } catch (error) {
        console.error('Error creating or updating target profile', error);
        usage();
        Bookshelf.knex.destroy();
        process.exit(5);
      }
    });
  } catch (error) {
    console.error('Error processing target profile', error);
    usage();
    Bookshelf.knex.destroy();
    process.exit(5);
  }

  Bookshelf.knex.destroy();
}

if (require.main === module) {
  main();
}

module.exports = {
  writeTargetProfile,
};

