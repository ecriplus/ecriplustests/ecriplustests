const { knex, disconnect } = require('../../db/knex-database-connection');
const logger = require('../../lib/infrastructure/logger');

async function _updateMaxReachablePixScore() {
  const result = await knex.raw(
    `
    UPDATE "certification-courses"
    SET "maxReachableScoreOnCertificationDate" = 736
    WHERE "createdAt" BETWEEN '2023-09-10' AND '2024-03-06';
  `
  );
  logger.info(result);
}

const isLaunchedFromCommandLine = require.main === module;

async function main() {
  await _updateMaxReachablePixScore();
}

(async () => {
  if (isLaunchedFromCommandLine) {
    try {
      await main();
    } catch (error) {
      logger.error(error);
      process.exitCode = 1;
    } finally {
      await disconnect();
    }
  }
})();