#! /usr/bin/env node
/* eslint no-console: ["off"] */

const encrypt = require('../lib/domain/services/encryption-service');
const authenticationMethodRepository = require('../lib/infrastructure/repositories/authentication-method-repository');

function usage() {
  console.log('Usage: ', 'change-user-password <user_id> <raw_password>');
}

function changeUserPassword(userid, rawPassword) {
  const hashedPassword = encrypt.hashPasswordSync(rawPassword);
  return authenticationMethodRepository.updateChangedPassword({
    userId: userid,
    hashedPassword,
  });
}

if (require.main === module) {
  if (process.argv.length != 4) {
    console.error('Wrong number of arguments.');
    usage();
    process.exit(1);
  }

  const [userid, rawPassword] = process.argv.slice(2);

  console.log(`Change user password ${userid}.`);

  changeUserPassword(userid, rawPassword)
    .catch((e) => {
      console.error('Error changing password:', e.message);
      process.exit(3);
    })
    .then(() => {
      console.log(`Password for user ${userid} updated.`);
      process.exit(0);
    });
}

module.exports = {
  changeUserPassword,
};
