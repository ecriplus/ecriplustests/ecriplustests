#! /usr/bin/env node
/* eslint no-console: ["off"] */

const encrypt = require('../lib/domain/services/encryption-service');
const emailValidator = require('../lib/domain/services/email-validator');
const DomainTransaction = require('../lib/infrastructure/DomainTransaction');
const AuthenticationMethod = require('../lib/domain/models/AuthenticationMethod');
const userToCreateRepository = require('../lib/infrastructure/repositories/user-to-create-repository');
const authenticationMethodRepository = require('../lib/infrastructure/repositories/authentication-method-repository');
const { ROLES } = require('../lib/domain/constants').PIX_ADMIN;

function usage() {
  console.log('Usage: ', 'create-superuser <email@ex.com> <username> <firstname> <lastname> <raw_password>');
}

function createSuperuser(domainTransaction, email, username, firstName, lastName, rawPassword) {
  const userValues = {
    email,
    username,
    firstName,
    lastName,
    lang: 'fr',
    cgu: true,
    lastTermsOfServiceValidatedAt: null,
    lastPixOrgaTermsOfServiceValidatedAt: null,
    lastPixCertifTermsOfServiceValidatedAt: null,
    mustValidateTermsOfService: false,
    pixOrgaTermsOfServiceAccepted: false,
    pixCertifTermsOfServiceAccepted: false,
    hasSeenAssessmentInstructions: false,
    hasSeenNewDashboardInfo: false,
    hasSeenFocusedChallengeTooltip: false,
    hasSeenOtherChallengesTooltip: false,
    isAnonymous: false,
  };

  const authMethodValues = {
    identityProvider: AuthenticationMethod.identityProviders.PIX,
    authenticationComplement: new AuthenticationMethod.PixAuthenticationComplement({
      // eslint-disable-next-line no-sync
      password: encrypt.hashPasswordSync(rawPassword),
      shouldChangePassword: false,
    }),
    externalIdentifier: undefined,
  };

  const trx = domainTransaction.knexTransaction;

  return Promise.all([
    userToCreateRepository.create({ user: userValues, domainTransaction }),
  ]).then(([user]) =>
    Promise.all([
      authenticationMethodRepository.create({
        authenticationMethod: { ...authMethodValues, userId: user.id },
        domainTransaction,
      }),
      trx('pix-admin-roles').insert({ userId: user.id, role: ROLES.SUPER_ADMIN }),
    ])
  );
}

if (require.main === module) {
  if (process.argv.length != 7) {
    console.error('Wrong number of arguments.');
    usage();
    process.exit(1);
  }

  const [email, username, firstName, lastName, rawPassword] = process.argv.slice(2);

  if (!emailValidator.emailIsValid(email)) {
    console.error(`Email ${email} not correct.`);
    usage();
    process.exit(2);
  }

  console.log(`Create Superuser ${username} ${firstName} ${lastName} with email ${email}.`);

  DomainTransaction.execute((domainTransaction) =>
    createSuperuser(domainTransaction, email, username, firstName, lastName, rawPassword)
  )
    .catch((e) => {
      console.error('Error creating Superuser:', e.message);
      process.exit(3);
    })
    .then(() => {
      console.log(`Superuser ${username} ${firstName} ${lastName} with email ${email} created.`);
      process.exit(0);
    });
}

module.exports = {
  createSuperuser,
};
