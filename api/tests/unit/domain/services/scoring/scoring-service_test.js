const scoringService = require('../../../../../lib/domain/services/scoring/scoring-service');
const config = require('../../../../../lib/config');

const { expect, domainBuilder } = require('../../../../test-helper');

describe('Unit | Service | Scoring Service', function () {
  describe('#calculateScoringInformationForCompetence', function () {
    it('should return the information about pix score and level for given competence', function () {
      // given
      const knowledgeElements = [
        domainBuilder.buildKnowledgeElement({ earnedPix: 3.7 }),
        domainBuilder.buildKnowledgeElement({ earnedPix: 4.4 }),
        domainBuilder.buildKnowledgeElement({ earnedPix: 1.2 }),
      ];

      const expectedScoring = {
        realTotalPixScoreForCompetence: 9.3,
        pixScoreForCompetence: 9,
        currentLevel: 1,
        pixAheadForNextLevel: 1,
      };

      // when
      const scoring = scoringService.calculateScoringInformationForCompetence({
        competenceId: 'recCOMP456',
        knowledgeElements,
      });

      // then
      expect(scoring).to.deep.equal(expectedScoring);
    });

    it('should return the information about pix score and level for one competence blocked with max information', function () {
      // given
      const knowledgeElements = [
        domainBuilder.buildKnowledgeElement({ earnedPix: config.features.maxReachablePixByCompetence['default'] }),
        domainBuilder.buildKnowledgeElement({ earnedPix: config.features.pixCountByLevel }),
        domainBuilder.buildKnowledgeElement({ earnedPix: config.features.pixCountByLevel }),
      ];

      const expectedScoring = {
        realTotalPixScoreForCompetence: 56,
        pixScoreForCompetence: config.features.maxReachablePixByCompetence['default'],
        currentLevel: config.features.maxReachableLevel['default'],
        pixAheadForNextLevel: 0,
      };

      // when
      const scoring = scoringService.calculateScoringInformationForCompetence({
        competenceId: 'default',
        knowledgeElements,
      });

      // then
      expect(scoring).to.be.deep.equal(expectedScoring);
    });

    context('when we allow an excess in pix or level', function () {
      it('should return the information about pix score and level for one competence blocked not blocked', function () {
        // given
        const knowledgeElements = [
          domainBuilder.buildKnowledgeElement({ earnedPix: config.features.maxReachablePixByCompetence['recCOMP456'] }),
          domainBuilder.buildKnowledgeElement({ earnedPix: config.features.pixCountByLevel }),
          domainBuilder.buildKnowledgeElement({ earnedPix: config.features.pixCountByLevel }),
        ];
        const allowExcessLevel = true;
        const allowExcessPix = true;
        const expectedScoring = {
          realTotalPixScoreForCompetence: 56,
          pixScoreForCompetence: 56,
          currentLevel: 7,
          pixAheadForNextLevel: 0,
        };

        // when
        const scoring = scoringService.calculateScoringInformationForCompetence({
          competenceId: 'recCOMP456',
          knowledgeElements,
          allowExcessLevel,
          allowExcessPix,
        });

        // then
        expect(scoring).to.be.deep.equal(expectedScoring);
      });
    });
  });

  describe('#calculatePixScore', function () {
    it('returns the Pix score and limit the score', function () {
      const knowledgeElements = [
        domainBuilder.buildKnowledgeElement({ competenceId: 'competence1', skillId: 'skill1.1', earnedPix: 8 }),
        domainBuilder.buildKnowledgeElement({ competenceId: 'competence1', skillId: 'skill1.2', earnedPix: 35 }),
        domainBuilder.buildKnowledgeElement({ competenceId: 'competence2', skillId: 'skill2.1', earnedPix: 1 }),
      ];

      expect(scoringService.calculatePixScore(knowledgeElements)).to.be.equal(41);
    });
  });
});
