const { expect } = require('../../test-helper');
const nock = require('nock');
const createServer = require('../../../server');

const learningContent = {
  skills: [
    {
      id: 'q_first_acquis',
      name: '@web3',
      hint_i18n: {
        fr: 'Geolocaliser ?',
        en: 'Geolocate ?',
      },
      hintStatus: 'Validé',
      competenceId: 'recABCD123',
      tutorialIds: ['english-tutorial-id', 'french-tutorial-id'],
      learningMoreTutorialIds: [],
      level: 5,
    },
  ],
};

describe('Unit | Server | server', function () {
  describe('#createServer', function () {
    beforeEach(function () {
      nock('https://lcms-test.pix.fr/api')
        .get('/releases/latest')
        .matchHeader('Authorization', 'Bearer test-api-key')
        .reply(200, { content: learningContent });
      nock.disableNetConnect();
    });

    afterEach(function () {
      nock.cleanAll();
    });

    it('should create server with custom validate.failAction', async function () {
      // given
      const expectedFailActionFunctionName = 'handleFailAction';

      // when
      const server = await createServer();

      // then
      expect(server.settings.routes.validate.failAction.name).to.equal(expectedFailActionFunctionName);
    });
  });
});
