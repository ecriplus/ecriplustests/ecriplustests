const {
  databaseBuilder,
  expect,
  knex,
  generateValidRequestAuthorizationHeader,
  insertUserWithRoleSuperAdmin,
} = require('../../../test-helper');
const createServer = require('../../../../server');
const Membership = require('../../../../lib/domain/models/Membership');
const { setResolveMx, clearResolveMx } = require('../../../../lib/infrastructure/mail-check');

describe('Acceptance | Route | Organizations', function () {
  describe('POST /api/admin/organizations/{id}/invitations', function () {
    beforeEach(async function () {
      setResolveMx(() => true);
    });
    afterEach(async function () {
      await knex('organization-invitations').delete();
      clearResolveMx();
    });

    it('should return 201 HTTP status code', async function () {
      // given
      const server = await createServer();

      const superAdmin = await insertUserWithRoleSuperAdmin();
      const organization = databaseBuilder.factory.buildOrganization();

      const payload = {
        data: {
          type: 'organization-invitations',
          attributes: {
            email: 'user1@organization.org',
            lang: 'fr',
            role: Membership.roles.ADMIN,
          },
        },
      };

      const options = {
        method: 'POST',
        url: `/api/admin/organizations/${organization.id}/invitations`,
        payload,
        headers: {
          authorization: generateValidRequestAuthorizationHeader(superAdmin.id),
        },
      };

      await databaseBuilder.commit();

      // when
      const response = await server.inject(options);

      // then
      expect(response.statusCode).to.equal(201);
    });
  });
});
