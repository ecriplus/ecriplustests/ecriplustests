const nock = require('nock');

//{"default" :8, "D-MCONJ": 3, "D-PDV": 4, "D-SSENT": 5, "D-MCONJ": 3, "M-CHOI": 4, "M-COMP": 5, "M-DEV": 3, "M-ORTHO": 4, "P-ART": 5, "P-CONS": 3, "P-GRAM": 4, "P-MOD": 5, "T-ENCH": 3, "T-ORG": 4, "T-REP": 5,},
const learningContent = {
  skills: [
    {
      id: 'q_first_acquis',
      name: '@web3',
      hint_i18n: {
        fr: 'Geolocaliser ?',
        en: 'Geolocate ?',
      },
      hintStatus: 'Validé',
      competenceId: 'recABCD123',
      tutorialIds: ['english-tutorial-id', 'french-tutorial-id'],
      learningMoreTutorialIds: [],
      level: 5,
      status: 'actif',
      pixValue: 1,
    },
  ],
};

exports.mochaHooks = {
  beforeEach(done) {
    nock('https://lcms-test.pix.fr/api')
      .get('/releases/latest')
      .matchHeader('Authorization', 'Bearer test-api-key')
      .reply(200, { content: learningContent });
    nock.disableNetConnect();
    done();
  },

  afterEach(done) {
    nock.cleanAll();
    done();
  },
};
