FROM dtzar/helm-kubectl:3.14.0

RUN apk --no-cache add \
      bash \ 
      coreutils \
      git \
      bash \
      py3-pip \
      wget \
      gnupg \
      jq \
      docker \
      git-crypt \
      gettext \
      openssl \
      curl \
      moreutils && \
    pip install yq --break-system-packages
