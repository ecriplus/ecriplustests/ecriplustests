FROM cimg/node:16.19.1-browsers

ENV DEBIAN_FRONTEND=noninteractive

USER root
RUN add-apt-repository ppa:savoury1/ffmpeg4
RUN add-apt-repository ppa:savoury1/chromium -y
RUN echo ' \
Package: * \
Pin: release o=LP-PPA-savoury1-chromium \
Pin-Priority: 1001 \
' | tee /etc/apt/preferences.d/savoury1-chromium

RUN apt-get update && apt-get install -y --no-install-recommends \
    chromium-browser \
    && rm -rf /var/lib/apt/lists/*
USER circleci
RUN npm install --save node-releases
RUN npm config set script-shell "/usr/bin/bash"