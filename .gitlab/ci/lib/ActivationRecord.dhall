let Stage = env:STAGE_DEF

let StageActivation
    : Type
    = { Test : Bool
      , Build : Bool
      , Deploy : Bool
      , CreateRelease : Bool
      , LoadSeeds : Bool
      , DeployAirtable : Bool
      }

let FirstStageList =
      { Type = StageActivation
      , default =
        { Test = False
        , Build = False
        , Deploy = False
        , CreateRelease = False
        , LoadSeeds = False
        , DeployAirtable = False
        }
      }

let LastStageList =
      { Type = StageActivation
      , default =
        { Test = True
        , Build = True
        , Deploy = True
        , CreateRelease = True
        , LoadSeeds = True
        , DeployAirtable = True
        }
      }

let isActive
    : StageActivation → StageActivation → Bool
    = λ(firstJobValues : StageActivation) →
      λ(lastJobValues : StageActivation) →
            merge firstJobValues Stage.firstJobStage
        &&  merge lastJobValues Stage.lastJobStage

in  { Test = isActive FirstStageList::{ Test = True } LastStageList::{=}
    , Build =
        isActive
          FirstStageList::{ Test = True, Build = True }
          LastStageList::{ Test = False }
    , Deploy =
        isActive
          FirstStageList::{ Test = True, Build = True, Deploy = True }
          LastStageList::{ Test = False, Build = False }
    , CreateRelease =
        isActive
          FirstStageList::{
          , Test = True
          , Build = True
          , Deploy = True
          , CreateRelease = True
          }
          LastStageList::{ Test = False, Build = False, Deploy = False }
    , LoadSeeds =
        isActive
          FirstStageList::{
          , Test = True
          , Build = True
          , Deploy = True
          , CreateRelease = True
          , LoadSeeds = True
          }
          LastStageList::{
          , Test = False
          , Build = False
          , Deploy = False
          , CreateRelease = False
          }
    , DeployAirtable =
        isActive
          FirstStageList::{
          , Test = True
          , Build = True
          , Deploy = True
          , CreateRelease = True
          , LoadSeeds = True
          , DeployAirtable = True
          }
          LastStageList::{
          , Test = False
          , Build = False
          , Deploy = False
          , CreateRelease = False
          , LoadSeeds = False
          }
    }
