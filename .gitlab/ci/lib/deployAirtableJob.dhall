let GitLab = ./GitLab.dhall

let commonSecretScript = ./commonSecretScript.dhall

let commonVariables = ./commonVariables.dhall

let JobEnvironment = ./jobEnvironment.dhall

in  GitLab.Job::{
    , stage = Some "deploy_airtable"
    , image = Some GitLab.Image::{ name = env:NODE_ALPINE_IMAGE as Text }
    , variables = toMap commonVariables.vars
    , before_script = Some
        (   commonSecretScript
          # [ "git clone https://gitlab-ci-token:\${CI_JOB_TOKEN}@gitlab.com/ecriplus/ecriplustests/tools.git -b \$TOOLS_BRANCH_NAME builds/ecriplus/tools"
            , "cd \"${env:CI_PROJECT_DIR as Text}/builds/ecriplus/tools\""
            , "npm install"
            ]
        )
    , script =
      [ "cd \"${env:CI_PROJECT_DIR as Text}/builds/ecriplus/tools\""
      , "node updateairtable"
      , "if [ \"\${IMPORT_SEEDS:-false}\" = true ]; then export PIXMASTER_EMAIL=\"\$SEEDS_PIXMASTER_EMAIL\"; export PIXMASTER_PASSWORD=\"\$SEEDS_PIXMASTER_PASSWORD\"; fi"
      , "export TOKEN=\$(curl --fail-with-body -X POST -H 'Content-Type:application/x-www-form-urlencoded' --data-urlencode \"grant_type=password\" --data-urlencode \"username=\${PIXMASTER_EMAIL}\" --data-urlencode \"password=\${PIXMASTER_PASSWORD}\" --data-urlencode \"scope=pix-admin\" \${API_HOST}/api/token | jq -r '.access_token')"
      , "curl --fail-with-body -X POST -s -H \"Authorization:Bearer \${TOKEN}\" \${API_HOST}/api/lcms/releases"
      ]
    , environment = JobEnvironment.environment
    , rules = Some
      [ GitLab.Rule::{
        , when = Some GitLab.When.Type.Manual
        , allow_failure = Some True
        }
      ]
    }
