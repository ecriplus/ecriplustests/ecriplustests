let GitLab = ./GitLab.dhall

let CommonVariables = ./commonVariables.dhall

in  GitLab.Job::{
    , stage = Some "test"
    , image = Some GitLab.Image::{ name = env:NODE_IMAGE as Text }
    , services = Some
      [ GitLab.Service::{
        , name = "redis:latest"
        , variables = toMap { LC_COLLATE = "C.utf8", LC_CTYPE = "C.utf8" }
        }
      , GitLab.Service::{ name = "postgres" }
      ]
    , variables = toMap
        (   CommonVariables.vars
          ∧ { REDIS_URL = "redis://redis"
            , TEST_REDIS_URL = "redis://redis"
            , POSTGRES_DB = "ecriplus"
            , POSTGRES_USER = "pguser"
            , POSTGRES_PASSWORD = "a2345f"
            , POSTGRES_HOST_AUTH_METHOD = "trust"
            , LC_COLLATE = "en_US.utf8"
            , LC_CTYPE = "en_US.utf8"
            }
        )
    , script = [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/test_ecriplustests.sh" ]
    }
