let GitLab = ./GitLab.dhall

let commonSecretScript = ./commonSecretScript.dhall

let commonVariables = ./commonVariables.dhall

let JobEnvironment = ./jobEnvironment.dhall

in  GitLab.Job::{
    , stage = Some "deploy"
    , image = Some GitLab.Image::{ name = env:NODE_ALPINE_IMAGE as Text }
    , variables = toMap commonVariables.vars
    , before_script = Some
        (   [ "set -x" ]
          # commonSecretScript
          # [ "kubectl config use-context \${K8S_CONTEXT}" ]
        )
    , script =
      [ "editor_pod_name=\"\$(kubectl -n \$K8S_NAMESPACE get pods --selector=\"app.kubernetes.io/name=ecripluseditor\" --sort-by=\"{.metadata.creationTimestamp}\" -o jsonpath=\"{.items[-1:].metadata.name}\")\""
      , "kubectl -n \$K8S_NAMESPACE wait pod \$editor_pod_name --for=condition=Ready --timeout=600s"
      , "rcommand=\"wget -O- -q --post-data=\\\"\\\" --header \\\"Authorization:Bearer \${LCMS_API_KEY}\\\" \\\"http://\\\$ECRIPLUSEDITOR_API_SERVICE_HOST:\\\$ECRIPLUSEDITOR_API_SERVICE_PORT/api/releases\\\"\""
      , "res=\$(kubectl -n \$K8S_NAMESPACE exec \$editor_pod_name -- bash -c \"\${rcommand}\")"
      , "if [ \"\$res\" = \"error\" ]; then exit 1; fi"
      ]
    , environment = JobEnvironment.environment
    , needs = Some
      [ GitLab.Needs::{
        , job = Some "deploy_ecripluseditor"
        , artifacts = True
        , optional = False
        }
      ]
    }
