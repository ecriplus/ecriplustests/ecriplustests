let GitLab = ./GitLab.dhall

let commonVariables = (./commonVariables.dhall).vars

in  GitLab.Job::{
    , stage = Some "build"
    , variables = toMap
        { DOCKER_BASE_REGISTRY =
            "\${CI_REGISTRY}/ecriplus/ecriplustests/ecripluseditor"
        , K8S_NAMESPACE = "\$K8S_NAMESPACE"
        , SUBDOMAIN = "\$SUBDOMAIN"
        , OPS_BRANCH = "${commonVariables.OPS_BRANCH_NAME}"
        , PARENT_BRANCH_NAME = "\$PARENT_BRANCH_NAME"
        , CI_FIRST_JOB = "build"
        , CI_LAST_JOB = "build"
        , DOCKER_IMAGE = env:DOCKER_IMAGE as Text
        , DOCKER_BUILDKIT = "1"
        }
    , trigger = Some GitLab.Trigger::{
      , project = Some "ecriplus/ecriplustests/ecripluseditor"
      , branch = Some "\$EDITOR_BRANCH"
      , strategy = Some GitLab.TriggerStrategy.Type.Depend
      , forward = Some GitLab.TriggerForward::{ yaml_variables = True }
      }
    , needs = Some
      [ GitLab.Needs::{
        , job = Some "editor_init"
        , artifacts = True
        , optional = False
        }
      ]
    }
