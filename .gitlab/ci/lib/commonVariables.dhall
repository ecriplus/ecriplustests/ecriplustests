let imageVersion = env:CI_COMMIT_TAG as Text ? env:CI_COMMIT_SHORT_SHA as Text

let branchName = "${env:CI_COMMIT_BRANCH as Text ? env:PROD_BRANCH as Text}"

in  { imageVersion
    , branchName
    , vars =
      { BRANCH_NAME = branchName
      , DOCKER_BUILDKIT = "1"
      , OPS_BRANCH_NAME =
          "${env:OPS_BRANCH as Text ? env:DEFAULT_OPS_BRANCH as Text}"
      , CHARTS_BRANCH_NAME =
          "${env:CHARTS_BRANCH as Text ? env:DEFAULT_CHARTS_BRANCH as Text}"
      , TOOLS_BRANCH_NAME =
          "${env:TOOLS_BRANCH as Text ? env:DEFAULT_TOOLS_BRANCH as Text}"
      , HOME_BRANCH_NAME =
          "${env:HOME_BRANCH as Text ? env:DEFAULT_HOME_BRANCH as Text}"
      , IMAGE_VERSION = imageVersion
      , IMAGE_TAG = "${Text/replace "+" "_" imageVersion}"
      , npm_config_registry =
          "${env:npm_config_registry as Text ? "https://registry.npmjs.org/"}"
      , npm_config_cache = "${env:CI_PROJECT_DIR as Text}/.npm"
      }
    }
