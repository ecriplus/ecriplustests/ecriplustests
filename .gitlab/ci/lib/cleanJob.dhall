let GitLab = ./GitLab.dhall

let commonSecretScript = ./commonSecretScript.dhall

let CommonVariables = ./commonVariables.dhall

in  GitLab.Job::{
    , stage = Some "clean"
    , allow_failure = True
    , image = Some GitLab.Image::{
      , name = env:HELM_IMAGE as Text
      , entrypoint = Some [ "" ]
      }
    , variables = toMap (CommonVariables.vars ∧ { GIT_STRATEGY = "none" })
    , before_script = Some
        (   [ ''
              if [[ ! -d "''${CI_PROJECT_DIR}/.git" ]]; then
                  cd /
                  rm -fr "''${CI_PROJECT_DIR}"
                  git clone -b master "''${CI_REPOSITORY_URL}" "''${CI_PROJECT_DIR}"
              fi
              ''
            , "cd \"\$CI_PROJECT_DIR\""
            , "git fetch"
            , "git checkout master"
            , "[[ -z \$(git ls-remote --heads https://gitlab-ci-token:\${CI_JOB_TOKEN}@gitlab.com/ecriplus/ops/ops.git refs/heads/\${OPS_BRANCH_NAME}) ]] && export OPS_BRANCH_NAME=master || :"
            ]
          # commonSecretScript
          # [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/activate_helm.sh" ]
        )
    , script = [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/clean.sh" ]
    , environment = Some GitLab.Environment::{
      , name = Some "rev/${env:CI_COMMIT_REF_NAME as Text}"
      , action = Some GitLab.EnvironmentAction.Type.Stop
      }
    , rules = Some
      [ GitLab.Rule::{
        , when = Some GitLab.When.Type.Manual
        , allow_failure = Some True
        }
      ]
    }
