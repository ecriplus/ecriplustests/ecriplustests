let GitLab = ./GitLab.dhall

let EpEnvironment = ./jobEnvironment.dhall

let listDropNones = (./utils.dhall).listDropNones

let commonVariables = (./commonVariables.dhall).vars

in  GitLab.Job::{
    , stage = Some "deploy"
    , variables = toMap
        { DOCKER_BASE_REGISTRY = "\${CI_REGISTRY}/ecriplus/misc/ecriplusaccueil"
        , K8S_NAMESPACE = "\$K8S_NAMESPACE"
        , SITE_NAME = "\$SUBDOMAIN"
        , BASE_URL = "\${SUBDOMAIN}.br.ecriplus.fr"
        , SUBDOMAIN = "\$SUBDOMAIN"
        , OPS_BRANCH = "${commonVariables.OPS_BRANCH_NAME}"
        , CHARTS_BRANCH = "${commonVariables.CHARTS_BRANCH_NAME}"
        , PARENT_BRANCH_NAME = "\$PARENT_BRANCH_NAME"
        , CI_FIRST_JOB = "deploy"
        , CI_LAST_JOB = "deploy"
        }
    , trigger = Some GitLab.Trigger::{
      , project = Some "ecriplus/misc/ecriplusaccueil"
      , branch = Some "${commonVariables.HOME_BRANCH_NAME}"
      , strategy = Some GitLab.TriggerStrategy.Type.Depend
      , forward = Some GitLab.TriggerForward::{ yaml_variables = True }
      }
    , needs = Some
        ( listDropNones
            GitLab.Needs.Type
            GitLab.Needs.default
            [ Some
                GitLab.Needs::{
                , job = Some "home_init"
                , artifacts = True
                , optional = False
                }
            , if    EpEnvironment.isDev
              then  Some
                      GitLab.Needs::{
                      , job = Some "deploy_init"
                      , artifacts = False
                      , optional = False
                      }
              else  None GitLab.Needs.Type
            ]
        )
    }
