let GitLab = ./GitLab.dhall

let commonSecretScript = ./commonSecretScript.dhall

let CommonVariables = ./commonVariables.dhall

let EpEnvironment = ./jobEnvironment.dhall

let JobRetryWhenFailure = GitLab.JobRetryWhenFailure.Type

let JobRetryWhen = GitLab.JobRetryWhen.Type

let JobRetry = GitLab.JobRetry.Type

in  GitLab.Job::{
    , stage = Some "deploy"
    , image = Some GitLab.Image::{
      , name = env:HELM_IMAGE as Text
      , entrypoint = Some [ "" ]
      }
    , variables = toMap CommonVariables.vars
    , before_script = Some
        (   commonSecretScript
          # [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/activate_helm.sh" ]
        )
    , script = [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/deploy_init.sh" ]
    , environment = EpEnvironment.environment
    , retry = Some
        ( JobRetry.WhenMax
            { max = Some 1
            , when = Some
                ( JobRetryWhen.List
                    [ JobRetryWhenFailure.RunnerSystemFailure
                    , JobRetryWhenFailure.ScriptFailure
                    , JobRetryWhenFailure.UnknownFailure
                    ]
                )
            }
        )
    }
