let GitLab = ./GitLab.dhall

let EpEnvironment = env:EP_ENVIRONMENT_DEF

let EpEnvironmentActivation =
      { Type = { Dev : Bool, Prod : Bool, Unknown : Bool }
      , default = { Dev = False, Prod = False, Unknown = False }
      }

let isProduction =
      merge
        EpEnvironmentActivation::{ Prod = True }
        EpEnvironment.epEnvironmentVal

let isDev =
      merge
        EpEnvironmentActivation::{ Dev = True }
        EpEnvironment.epEnvironmentVal

in  { isDev
    , isProduction
    , environment =
        if    isProduction
        then  Some
                GitLab.Environment::{
                , name = Some env:PROD_BRANCH as Text
                , url = Some "https://ecriplustests.ecriplus.fr"
                }
        else  if isDev
        then  Some
                GitLab.Environment::{
                , name = Some "rev/${env:CI_COMMIT_REF_NAME as Text}"
                , url = Some
                    "https://${  env:CI_ENVIRONMENT_SLUG as Text
                               ? "NONE"}.br.ecriplus.fr"
                , on_stop = Some "stop"
                }
        else  None GitLab.Environment.Type
    }
