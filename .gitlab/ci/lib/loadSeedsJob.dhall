let GitLab = ./GitLab.dhall

let commonSecretScript = ./commonSecretScript.dhall

let CommonVariables = ./commonVariables.dhall

let JobEnvironment = ./jobEnvironment.dhall

in  GitLab.Job::{
    , stage = Some "load_seeds"
    , image = Some GitLab.Image::{
      , name = env:HELM_IMAGE as Text
      , entrypoint = Some [ "" ]
      }
    , variables = toMap CommonVariables.vars
    , before_script = Some
        (   [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/common_values.sh" ]
          # commonSecretScript
          # [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/activate_helm.sh" ]
        )
    , script = [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/load_seeds.sh" ]
    , environment = JobEnvironment.environment
    , needs = Some
      [ GitLab.Needs::{
        , job = Some "deploy_ecriplustests"
        , artifacts = False
        , optional = False
        }
      ]
    }
