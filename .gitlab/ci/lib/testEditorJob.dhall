let GitLab = ./GitLab.dhall

let commonVariables = (./commonVariables.dhall).vars

in  GitLab.Job::{
    , stage = Some "test"
    , variables = toMap
        { DOCKER_BASE_REGISTRY =
            "\${CI_REGISTRY}/ecriplus/ecriplustests/ecripluseditor"
        , OPS_BRANCH = commonVariables.OPS_BRANCH_NAME
        , CI_FIRST_JOB = "test"
        , CI_LAST_JOB = "test"
        }
    , trigger = Some GitLab.Trigger::{
      , project = Some "ecriplus/ecriplustests/ecripluseditor"
      , branch = Some "\$EDITOR_BRANCH"
      , strategy = Some GitLab.TriggerStrategy.Type.Depend
      , forward = Some GitLab.TriggerForward::{ yaml_variables = True }
      }
    , needs = Some
      [ GitLab.Needs::{
        , job = Some "editor_init"
        , artifacts = True
        , optional = False
        }
      ]
    }
