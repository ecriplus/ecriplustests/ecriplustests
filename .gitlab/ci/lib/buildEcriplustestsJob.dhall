let GitLab = ./GitLab.dhall

let commonSecretScript = ./commonSecretScript.dhall

let CommonVariables = ./commonVariables.dhall

let EpEnvironment = ./jobEnvironment.dhall

in  GitLab.Job::{
    , stage = Some "build"
    , image = Some GitLab.Image::{ name = env:DOCKER_IMAGE as Text }
    , services = Some [ GitLab.Service.mkService "docker:20.10.16-dind" ]
    , variables = toMap
        (CommonVariables.vars ∧ { BRANCH = CommonVariables.branchName })
    , before_script = Some commonSecretScript
    , script =
      [ "[[ \"\$CI_COMMIT_SHORT_SHA\" == \"\$IMAGE_VERSION\" ]] && IMAGE_VERSION=\"\$(cat package.json | jq -r \".version\")-\${IMAGE_VERSION}\""
      , "cd \"\$CI_PROJECT_DIR/builds/ecriplus/ops/docker/ecriplustests\""
      , "./build.sh -v \"\$IMAGE_VERSION\" -t \"\$IMAGE_TAG\" \"\$CI_PROJECT_DIR\""
      ]
    , environment = EpEnvironment.environment
    }
