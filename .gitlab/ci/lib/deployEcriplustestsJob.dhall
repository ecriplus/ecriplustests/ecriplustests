let GitLab = ./GitLab.dhall

let commonSecretScript = ./commonSecretScript.dhall

let CommonVariables = ./commonVariables.dhall

let EpEnvironment = ./jobEnvironment.dhall

let JobRetryWhenFailure = GitLab.JobRetryWhenFailure.Type

let JobRetryWhen = GitLab.JobRetryWhen.Type

let JobRetry = GitLab.JobRetry.Type

let listDropNones = (./utils.dhall).listDropNones

in  GitLab.Job::{
    , stage = Some "deploy"
    , image = Some GitLab.Image::{
      , name = env:HELM_IMAGE as Text
      , entrypoint = Some [ "" ]
      }
    , variables = toMap CommonVariables.vars
    , before_script = Some
        (   [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/common_values.sh" ]
          # commonSecretScript
          # [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/activate_helm.sh" ]
        )
    , script =
      [ ". \$CI_PROJECT_DIR/.gitlab/ci/scripts/deploy_ecriplustests.sh" ]
    , environment = EpEnvironment.environment
    , needs = Some
        ( listDropNones
            GitLab.Needs.Type
            GitLab.Needs.default
            [ Some
                GitLab.Needs::{
                , job = Some "build_ecriplustests"
                , optional = False
                }
            , if    EpEnvironment.isDev
              then  Some
                      GitLab.Needs::{
                      , job = Some "deploy_init"
                      , artifacts = True
                      , optional = False
                      }
              else  None GitLab.Needs.Type
            , if    EpEnvironment.isProduction
              then  Some
                      GitLab.Needs::{
                      , job = Some "deploy_ecripluseditor"
                      , artifacts = True
                      , optional = False
                      }
              else  None GitLab.Needs.Type
            , Some
                GitLab.Needs::{
                , job = Some "create_release"
                , artifacts = False
                , optional = False
                }
            ]
        )
    , retry = Some
        ( JobRetry.WhenMax
            { max = Some 2
            , when = Some
                ( JobRetryWhen.List
                    [ JobRetryWhenFailure.RunnerSystemFailure
                    , JobRetryWhenFailure.ScriptFailure
                    , JobRetryWhenFailure.UnknownFailure
                    ]
                )
            }
        )
    }
