let GitLab = ./GitLab.dhall

let CacheKey = GitLab.CacheKey.Type

in  GitLab.CacheSpec::{
    , paths = [ ".npm/" ]
    , key = Some
        (CacheKey.Text "${env:CACHE_KEY as Text ? env:CI_COMMIT_SHA as Text}")
    }
