let GitLab = ./GitLab.dhall

let Optional/default = GitLab.Prelude.Optional.default

let Optional/null = GitLab.Prelude.Optional.null

let List/filter = GitLab.Prelude.List.filter

let List/map = GitLab.Prelude.List.map

let listDropNones
    : ∀(a : Type) → a → List (Optional a) → List a
    = λ(a : Type) →
      λ(o : a) →
      λ(l : List (Optional a)) →
        let aIsNotNull = λ(val : Optional a) → Optional/null a val == False

        let aFilter = List/filter (Optional a) aIsNotNull

        let aDefault = Optional/default a o

        in  List/map (Optional a) a aDefault (aFilter l)

in  { listDropNones }
