let GitLab = ./GitLab.dhall

let commonSecretScript = ./commonSecretScript.dhall

let CommonVariables = ./commonVariables.dhall

let EpEnvironment = ./jobEnvironment.dhall

in  GitLab.Job::{
    , stage = Some "test"
    , image = Some GitLab.Image::{ name = env:DOCKER_IMAGE as Text }
    , services = Some [ GitLab.Service.mkService "docker:20.10.16-dind" ]
    , variables = toMap CommonVariables.vars
    , before_script = Some commonSecretScript
    , script =
      [ "echo \"EDITOR_BRANCH=\$EDITOR_BRANCH\" >> \$CI_PROJECT_DIR/editor.env"
      , "echo \"SUBDOMAIN=\$SUBDOMAIN\" >> \$CI_PROJECT_DIR/editor.env"
      , "echo \"K8S_NAMESPACE=\$K8S_NAMESPACE\" >> \$CI_PROJECT_DIR/editor.env"
      , "echo \"PARENT_BRANCH_NAME=\$BRANCH_NAME\" >> \$CI_PROJECT_DIR/editor.env"
      , "echo \"OPS_BRANCH=\${EDITOR_OPS_BRANCH:-\${OPS_BRANCH}}\" >> \$CI_PROJECT_DIR/editor.env"
      ]
    , artifacts = Some GitLab.ArtifactsSpec::{
      , reports = Some GitLab.Reports::{ dotenv = Some "editor.env" }
      }
    , environment = EpEnvironment.environment
    }
