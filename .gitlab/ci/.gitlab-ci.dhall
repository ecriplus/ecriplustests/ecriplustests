let GitLab = ./lib/GitLab.dhall

let EpEnvironment = ./lib/jobEnvironment.dhall

let ActivationRecord = ./lib/ActivationRecord.dhall

let cacheListDef = ./lib/cacheEcriplustests.dhall

let isDeploy = env:IS_DEPLOY

let pipelineSource = env:EP_PIPELINE_SOURCE

let GitStrategy = GitLab.GitStrategy

let Prelude = GitLab.Prelude

let renderTop = GitLab.Top.toJSON

let isPipelineSourceWeb =
      merge GitLab.PipelineSource.Activation::{ web = True } pipelineSource

let isNotProdBranch = env:EP_IS_PROD_BRANCH != True

let jobRecord =
      { test_ecriplustests =
          if    ActivationRecord.Test
          then  Some ./lib/testEcriplustestsJob.dhall
          else  None GitLab.Job.Type
      , editor_init =
          if    ActivationRecord.Build && isDeploy && isNotProdBranch
          then  Some ./lib/editorInitJob.dhall
          else  None GitLab.Job.Type
      , home_init =
          if        ActivationRecord.Build
                &&  EpEnvironment.isDev
                &&  isNotProdBranch
                &&  isDeploy
          then  Some ./lib/homeInitJob.dhall
          else  None GitLab.Job.Type
      , test_ecripluseditor =
          if    ActivationRecord.Test && isDeploy && isNotProdBranch
          then  Some ./lib/testEditorJob.dhall
          else  None GitLab.Job.Type
      , build_ecriplustests =
          if    ActivationRecord.Build && isDeploy && isNotProdBranch
          then  Some ./lib/buildEcriplustestsJob.dhall
          else  None GitLab.Job.Type
      , build_ecripluseditor =
          if    ActivationRecord.Build && isDeploy && isNotProdBranch
          then  Some ./lib/buildEditorJob.dhall
          else  None GitLab.Job.Type
      , deploy_init =
          if        ActivationRecord.Deploy
                &&  isDeploy
                &&  EpEnvironment.isDev
                &&  isNotProdBranch
          then  Some ./lib/deployInitJob.dhall
          else  None GitLab.Job.Type
      , deploy_ecripluseditor =
          if    ActivationRecord.Deploy && isDeploy && isNotProdBranch
          then  Some ./lib/deployEditorJob.dhall
          else  None GitLab.Job.Type
      , create_release =
          if    ActivationRecord.CreateRelease && isNotProdBranch && isDeploy
          then  Some ./lib/createReleaseJob.dhall
          else  None GitLab.Job.Type
      , deploy_ecriplustests =
          if    ActivationRecord.Deploy && isDeploy && isNotProdBranch
          then  Some ./lib/deployEcriplustestsJob.dhall
          else  None GitLab.Job.Type
      , load_seeds =
          if        ActivationRecord.LoadSeeds
                &&  isNotProdBranch
                &&  EpEnvironment.isDev
                &&  isDeploy
          then  Some ./lib/loadSeedsJob.dhall
          else  None GitLab.Job.Type
      , deploy_home =
          if        ActivationRecord.Deploy
                &&  EpEnvironment.isDev
                &&  isNotProdBranch
                &&  isDeploy
          then  Some ./lib/deployHomeJob.dhall
          else  None GitLab.Job.Type
      , deploy_airtable =
          if        ActivationRecord.DeployAirtable
                &&  isPipelineSourceWeb
                &&  isNotProdBranch
                &&  isDeploy
          then  Some ./lib/deployAirtableJob.dhall
          else  None GitLab.Job.Type
      , stop =
          if    EpEnvironment.isDev && isDeploy && isNotProdBranch
          then  Some ./lib/cleanJob.dhall
          else  None GitLab.Job.Type
      }

let top =
      GitLab.Top::{
      , stages =
          if    isDeploy
          then  Some
                  [ "test"
                  , "build"
                  , "deploy"
                  , "load_seeds"
                  , "deploy_airtable"
                  , "clean"
                  ]
          else  Some [ "test" ]
      , gitStrategy = Some GitStrategy.Type.Clone
      , variables = toMap { OPS_BRANCH = env:OPS_BRANCH as Text ? "master" }
      , default = Some GitLab.Defaults::{ cache = Some cacheListDef }
      , jobs =
          Prelude.Map.unpackOptionals Text GitLab.Job.Type (toMap jobRecord)
      }

in  Prelude.JSON.renderYAML (renderTop top)
