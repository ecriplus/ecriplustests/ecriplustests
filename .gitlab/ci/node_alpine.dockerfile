FROM node:alpine

RUN apk --no-cache add \
      bash\
      coreutils \
      git \
      py3-pip \
      wget \
      gnupg \
      jq \
      docker \
      git-crypt \
      gettext \
      curl \
      kubectl \
      moreutils && \
    pip install --break-system-packages  yq

