[[ "${EP_CI_DEBUG:-'false'}"  =~ "^[tT]rue|1$" ]] && set -x || :

cd "$CI_PROJECT_DIR/.gitlab/ci"
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
# docker
docker build -t "$DOCKER_TAG" -f docker.dockerfile .
docker push "$DOCKER_TAG"
# node alpine
docker build -t "$NODE_ALPINE_TAG" -f node_alpine.dockerfile .
docker push "$NODE_ALPINE_TAG"
# Dhall
docker build -t "$DHALL_TAG" --build-arg DHALL_BIN_URL="${DHALL_BIN_URL}" -f dhall.dockerfile .
docker push "$DHALL_TAG"
# Helm
docker build -t "$HELM_TAG" -f helm.dockerfile .
docker push "$HELM_TAG"
# Node
docker build -t "$NODE_TAG" -f node.dockerfile .
docker push "$NODE_TAG"
