[[ "${EP_CI_DEBUG:-'false'}"  =~ "^[tT]rue|1$" ]] && set -x || :

source_env() {
    set -a
    source "$1"
    set +a
}

if [ $BRANCH_NAME != $PROD_BRANCH ]; then
  export SUBDOMAIN=$CI_ENVIRONMENT_SLUG
  export K8S_NAMESPACE=$SUBDOMAIN
else
  export K8S_NAMESPACE="ecriplustests"
fi

echo -n $CI_REGISTRY_PASSWORD | docker login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
gpg --import --batch $GPG_PRIVATE_KEY
echo -e "5\ny\n" | gpg --no-tty --command-fd 0 --edit-key "$GPG_ID" trust
echo "Cloning OPS with branch $OPS_BRANCH_NAME"
git clone -b $OPS_BRANCH_NAME https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/ecriplus/ops/ops.git $CI_PROJECT_DIR/builds/ecriplus/ops
git config --global --add safe.directory "*"
cd $CI_PROJECT_DIR/builds/ecriplus/ops
git-crypt unlock

# values-ci toujours en premier
TYPES="values-ci secrets-ci values secrets"

 if [ $BRANCH_NAME != $PROD_BRANCH ]; then
  folder="dev"
else
  folder="$PROD_BRANCH"
fi

for type in $TYPES; do
  folder_type="$CI_PROJECT_DIR/builds/ecriplus/ops/branches/$folder/$type"
  if [ -e $folder_type/common-$type.ini ]; then
    source_env $folder_type/common-$type.ini
  fi
  if [ -e $folder_type/ecriplustests-$type.ini ]; then
    type_tmp_file="$(mktemp -p ${TMPDIR:-/tmp} ${type}_XXXXX.ini)"
    $CI_PROJECT_DIR/builds/ecriplus/ops/scripts/bash/complete-ci-ini.sh \
      -i $folder_type/ecriplustests-$type.ini \
      -y $folder_type/ecriplustests-$type.yaml \
      -o $type_tmp_file
    source_env $type_tmp_file
    [ -e "$type_tmp_file" ] && rm -f "$type_tmp_file" 
    if [ -e $folder_type/ecriplustests-$type.yaml ]; then
      envsubst < $folder_type/ecriplustests-$type.yaml | sponge $folder_type/ecriplustests-$type.yaml
    fi
  fi
done
cd $CI_PROJECT_DIR