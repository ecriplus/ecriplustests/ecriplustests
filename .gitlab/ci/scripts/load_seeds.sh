[[ "${EP_CI_DEBUG:-'false'}" =~ "^[tT]rue|1$" ]] && set -x || :

kubectl config use-context ${K8S_CONTEXT}

# This test should not be necessary, to get rid of when make sur loadSeeds job is not called on master
if [ $BRANCH_NAME != $PROD_BRANCH ]; then
    echo "-----> Seeds"
    seed_deployed=$(kubectl -n $K8S_NAMESPACE get jobs -o=jsonpath='{.items[?(@.metadata.name=="ecriplustests-seeds-job")].status.conditions[?(@.type=="Complete")].status}')
    if [[ $(kubectl -n $K8S_NAMESPACE get jobs --field-selector metadata.name=="ecriplustests-seeds-job" -o name) ]]; then
        if [[ "$seed_deployed" != "True" ]]; then
            echo "~~~~~ Deploying Seeds"
            kubectl patch -n $K8S_NAMESPACE job.batch/ecriplustests-seeds-job --type=strategic --patch '{"spec":{"suspend":false}}'
        fi
        echo "-----> Target profiles"
        kubectl wait -n $K8S_NAMESPACE --for=condition=complete --timeout=300s job.batch/ecriplustests-seeds-job
        $CI_PROJECT_DIR/builds/ecriplus/ops/scripts/bash/create-or-update-target-profiles.sh -n $K8S_NAMESPACE $CI_PROJECT_DIR/builds/ecriplus/ops/datas/profile_types/tests/dev_profile_types.json
    fi
fi
