[[ "${EP_CI_DEBUG:-'false'}"  =~ "^[tT]rue|1$" ]] && set -x || :

kubectl config use-context $K8S_CONTEXT
echo -e "apiVersion: v1\nkind: Namespace\nmetadata:\n  name: \"$K8S_NAMESPACE\"" | kubectl apply -f -

if [ $BRANCH_NAME != $PROD_BRANCH ]; then
  helm upgrade -i --set "namespace=${K8S_NAMESPACE}" deployuser-${K8S_NAMESPACE} repo-ecriplus/gitlab-deploy
else
  helm upgrade -i --set "namespace=${K8S_NAMESPACE}" deployuser repo-ecriplus/gitlab-deploy
fi

if ! $CI_PROJECT_DIR/builds/ecriplus/ops/scripts/bash/ns_tools.sh -k $NS_APPLICATION_KEY -s $NS_APPLICATION_SECRET -c $NS_CONSUMER_KEY exists $SUBDOMAIN; then
    $CI_PROJECT_DIR/builds/ecriplus/ops/scripts/bash/ns_tools.sh -k $NS_APPLICATION_KEY -s $NS_APPLICATION_SECRET -c $NS_CONSUMER_KEY add $SUBDOMAIN $NS_IP_OR_HOST
fi
