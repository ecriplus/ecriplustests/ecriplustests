#!/usr/bin/env bash
[[ "${EP_CI_DEBUG:-'false'}"  =~ "^[tT]rue|1$" ]] && set -x || :

cd admin && npm ci && npx browserslist@latest --update-db && cd ../
cd certif && npm ci && npx browserslist@latest --update-db && cd ../
cd api && npm ci && cd ../
cd mon-pix && npm ci && npx browserslist@latest --update-db && cd ../
cd orga && npm ci && npx browserslist@latest --update-db && cd ../
cd scripts && npm ci && cd ../

export DATABASE_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@postgres:5432/${POSTGRES_DB}"
export TEST_DATABASE_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@postgres:5432/${POSTGRES_DB}_test"
 
cd api && cp -f sample.env .env && npm run test && cd ../
npm run test:mon-pix
npm run test:orga
npm run test:scripts
npm run test:admin
npm run test:certif
