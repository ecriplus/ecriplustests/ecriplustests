[[ "${EP_CI_DEBUG:-'false'}" =~ "^[tT]rue|1$" ]] && set -x || :

kubectl config use-context ${K8S_CONTEXT}

CONFIG_VALUES=()
CONFIG_SECRETS=()
ops_branches_path="$CI_PROJECT_DIR/builds/ecriplus/ops/branches"
ops_scripts_path="$CI_PROJECT_DIR/builds/ecriplus/ops/scripts"
if [ $BRANCH_NAME != $PROD_BRANCH ]; then
    CONFIG_VALUES+=(-f "${ops_branches_path}/dev/values/ecriplustests-values.yaml")
    CONFIG_SECRETS+=(-f "${ops_branches_path}/dev/secrets/ecriplustests-secrets.yaml")
else
    CONFIG_VALUES+=(-f "${ops_branches_path}/${PROD_BRANCH}/values/ecriplustests-values.yaml")
    CONFIG_SECRETS+=(-f "${ops_branches_path}/${PROD_BRANCH}/secrets/ecriplustests-secrets.yaml")
fi

if [ -f "${ops_branches_path}/${IMAGE_TAG}/values/ecriplustests-values.yaml" ]; then
    CONFIG_VALUES+=(-f "${ops_branches_path}/${IMAGE_TAG}/values/ecriplustests-values.yaml")
fi

if [ -f "${ops_branches_path}/${IMAGE_TAG}/secrets/ecriplustests-secrets.yaml" ]; then
    CONFIG_SECRETS+=(-f "${ops_branches_path}/${IMAGE_TAG}/secrets/ecriplustests-secrets.yaml")
fi

echo "--> check that seeds job does not exists"
create_seeds_job_option=""
if [[ $(kubectl -n $K8S_NAMESPACE get jobs --field-selector metadata.name=="ecriplustests-seeds-job" -o name) ]]; then
    seed_deployed=$(kubectl -n $K8S_NAMESPACE get jobs -o=jsonpath='{.items[?(@.metadata.name=="ecriplustests-seeds-job")].status.conditions[?(@.type=="Complete")].status}')
    if [[ "$seed_deployed" != "True" ]]; then
        echo "~~~~~ Delete Seeds job"
        kubectl delete -n $K8S_NAMESPACE --force job.batch/ecriplustests-seeds-job
    else
        create_seeds_job_option="--set api.seed=false"
    fi
fi 


helm upgrade -i --force -n $K8S_NAMESPACE \
    ${CONFIG_VALUES[@]} \
    ${CONFIG_SECRETS[@]} \
    --set "image.app.version=$IMAGE_TAG" --set "image.api.version=$IMAGE_TAG" --set "image.eval.version=$IMAGE_TAG" \
    --set "image.admin.version=$IMAGE_TAG" --set "image.certif.version=$IMAGE_TAG" --set "image.backup.version=$IMAGE_TAG" \
    --set "image.maintenance.version=$IMAGE_TAG" \
    $create_seeds_job_option \
    ecriplustests repo-ecriplus/ecriplustests

# Check deployed version
api_pod_name="$(kubectl -n $K8S_NAMESPACE get pods --selector="app.kubernetes.io/name=ecriplustests-api" -o jsonpath="{.items[0].metadata.name}")"
kubectl -n $K8S_NAMESPACE wait pod $api_pod_name --for=condition=Ready --timeout=600s

if [ $BRANCH_NAME != $PROD_BRANCH ]; then
    echo "-----> Superuser"
    $CI_PROJECT_DIR/builds/ecriplus/ops/scripts/bash/create-superuser.sh -n $K8S_NAMESPACE $PIXMASTER_EMAIL pixmaster pixmaster pixmaster $PIXMASTER_PASSWORD || [ $? -eq 3 ]
    echo "-----> Cities / countries"
    $CI_PROJECT_DIR/builds/ecriplus/ops/scripts/bash/import-certification-cpf-cities-countries.sh -n $K8S_NAMESPACE
    echo "https://$SUBDOMAIN.br.ecriplus.fr"
fi
