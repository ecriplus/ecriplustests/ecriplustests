[[ "${EP_CI_DEBUG:-'false'}"  =~ "^[tT]rue|1$" ]] && set -x || :

helm repo add --force-update stable https://charts.helm.sh/stable
helm repo add --force-update bitnami https://charts.bitnami.com/bitnami
helm repo add --force-update --username gitlab-ci-token --password $CI_JOB_TOKEN repo-ecriplus ${CI_API_V4_URL}/projects/${HELM_PROJECT_ID}/packages/helm/${CHARTS_BRANCH_NAME}
