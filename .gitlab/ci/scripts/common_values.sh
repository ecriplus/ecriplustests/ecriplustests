[[ "${EP_CI_DEBUG:-'false'}"  =~ "^[tT]rue|1$" ]] && set -x || :
[[ "$CI_COMMIT_BRANCH" != "$PROD_BRANCH" ]] \
    && [[ -z ${OPS_BRANCH+x} ]] \
    && git ls-remote --exit-code --heads https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/ecriplus/ops/ops.git "refs/heads/$CI_COMMIT_BRANCH" \
    && export OPS_BRANCH="${CI_COMMIT_BRANCH}" \
    || :
[[ "$CI_COMMIT_BRANCH" != "$PROD_BRANCH" ]] \
    && [[ -z ${CHARTS_BRANCH+x} ]] \
    && git ls-remote --exit-code --heads https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/ecriplus/ops/charts.git "refs/heads/$CI_COMMIT_BRANCH" \
    && export CHARTS_BRANCH="${CI_COMMIT_BRANCH}" \
    || :


export BRANCH_NAME="${CI_COMMIT_BRANCH:-${PROD_BRANCH}}"
export OPS_BRANCH_NAME="${OPS_BRANCH:-${DEFAULT_OPS_BRANCH}}"
export CHARTS_BRANCH_NAME="${CHARTS_BRANCH:-${DEFAULT_CHARTS_BRANCH}}"
export TOOLS_BRANCH_NAME="${TOOLS_BRANCH:-${DEFAULT_TOOLS_BRANCH}}"
export IMAGE_VERSION="${CI_COMMIT_TAG:-$CI_COMMIT_SHORT_SHA}"
export IMAGE_TAG="${IMAGE_VERSION/+/_}"
export EP_ENVIRONMENT="$([[ $CI_COMMIT_TAG = v* ]] && echo 'Prod' || ( [[ -n $CI_COMMIT_BRANCH ]] && echo 'Dev' || echo 'Unknown' ))"
export EP_ENVIRONMENT_DEF="let EpEnvironment : Type = < Dev | Prod | Unknown > in { epEnvironmentVal = EpEnvironment.$EP_ENVIRONMENT }"
export EP_IS_PROD_BRANCH="$([[ $CI_COMMIT_BRANCH == $PROD_BRANCH ]] && echo 'True' || echo 'False')"

echo "BRANCH_NAME = $BRANCH_NAME"
echo "OPS_BRANCH_NAME = $OPS_BRANCH_NAME"
echo "CHARTS_BRANCH_NAME = $CHARTS_BRANCH_NAME"
echo "TOOLS_BRANCH_NAME = $TOOLS_BRANCH_NAME"
echo "IMAGE_VERSION = $IMAGE_VERSION"
echo "IMAGE_TAG = $IMAGE_TAG"
echo "EP_ENVIRONMENT = $EP_ENVIRONMENT"
echo "EP_ENVIRONMENT_DEF = $EP_ENVIRONMENT_DEF"
echo "EP_IS_PROD_BRANCH = $EP_IS_PROD_BRANCH"