[[ "${EP_CI_DEBUG:-'false'}"  =~ "^[tT]rue|1$" ]] && set -x || :

echo "Remove review app"
kubectl config use-context ${K8S_CONTEXT}
helm uninstall -n ${K8S_NAMESPACE} ecriplustests
helm uninstall -n ${K8S_NAMESPACE} ecripluseditor || :
helm uninstall -n ${K8S_NAMESPACE} ecriplusaccueil || :
echo -e "apiVersion: v1\nkind: Namespace\nmetadata:\n  name: '${K8S_NAMESPACE}'" | kubectl delete -f -

if [ $BRANCH_NAME != $PROD_BRANCH ]; then
  if $CI_PROJECT_DIR/builds/ecriplus/ops/scripts/bash/ns_tools.sh -k $NS_APPLICATION_KEY -s $NS_APPLICATION_SECRET -c $NS_CONSUMER_KEY exists $SUBDOMAIN; then
    $CI_PROJECT_DIR/builds/ecriplus/ops/scripts/bash/ns_tools.sh -k $NS_APPLICATION_KEY -s $NS_APPLICATION_SECRET -c $NS_CONSUMER_KEY delete $SUBDOMAIN
  fi
fi
