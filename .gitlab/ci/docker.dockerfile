FROM docker:20.10.16

RUN apk --no-cache add \
      bash \ 
      coreutils \
      git \
      curl \
      bash \
      py3-pip \
      wget \
      gnupg \
      jq \
      docker \
      git-crypt \
      gettext \
      moreutils && \
    pip install yq
