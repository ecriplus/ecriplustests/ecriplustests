FROM alpine:latest
ARG DHALL_BIN_URL

RUN apk --no-cache add \
        coreutils \
        moreutils \
        gettext \
        git-crypt \
        docker \
        jq \
        gnupg \
        wget \
        py3-pip \
        git \
        curl \
        bzip2 \
        bash && \
    pip install --break-system-packages yq && \
    curl -s -L "$DHALL_BIN_URL" | tar -xj
