### Résumé

(Résumer avec concision le problème rencontré)


### Étapes pour reproduire le problème

(important : indiquer les étapes permettant de reproduire le problème)


### Description du problème

(Ce qui se passe effectivement)


### Quel est le résultat attendu

(Ce que l'on devrait obtenir normalement)


### Captures d'écran

(Si possible, attacher à ce formulaire des captures d'écran montrant le problème)


/label ~bug
