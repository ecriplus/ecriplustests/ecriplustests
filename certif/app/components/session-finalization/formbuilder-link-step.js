import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import config from 'pix-certif/config/environment';
import template from 'lodash/template';

export default class FormbuilderLinkStep extends Component {
  @service currentUser;

  formBuilderLinkUrlTemplate = template(config.formBuilderLinkUrl);

  get certificationCenterNameAndExternalId() {
  }

  get linkTo() {
    const session = this.args.session;
    const allowedCertificationCenterAccess = this.currentUser.currentAllowedCertificationCenterAccess;
    const certificationCenter = allowedCertificationCenterAccess.externalId ?
      `${allowedCertificationCenterAccess.name} (${allowedCertificationCenterAccess.externalId})`:
      allowedCertificationCenterAccess.name;

    return this.formBuilderLinkUrlTemplate({
      sessionId: encodeURIComponent(session.id),
      certificationCenter: encodeURIComponent(certificationCenter),
      sessionDate: encodeURIComponent(session.date),
      sessionTime: encodeURIComponent(session.time),
    });
  }
}
