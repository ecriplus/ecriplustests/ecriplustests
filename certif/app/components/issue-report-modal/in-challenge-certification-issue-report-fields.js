import Component from '@glimmer/component';
import { action } from '@ember/object';
import {
  certificationIssueReportSubcategories,
  subcategoryToCode,
  subcategoryToLabel,
} from 'pix-certif/models/certification-issue-report';

export default class InChallengeCertificationIssueReportFields extends Component {
  @action
  onChangeSubcategory(option) {
    this.args.inChallengeCategory.subcategory = option;
  }

  get categoryCode() {
    return this.args.inChallengeCategory.categoryCode;
  }

  options = [
    'SOFTWARE_NOT_WORKING',
    'EXTRA_TIME_EXCEEDED',
  ]
    .map((subcategoryKey) => {
      const subcategory = certificationIssueReportSubcategories[subcategoryKey];
      const labelForSubcategory = subcategoryToLabel[subcategory];
      return {
        value: certificationIssueReportSubcategories[subcategory],
        label: `${subcategoryToCode[subcategory]} ${labelForSubcategory}`,
      };
    })
    .filter(Boolean);
}
