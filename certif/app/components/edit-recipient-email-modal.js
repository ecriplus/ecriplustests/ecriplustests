import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class EditRecipientEmailModal extends Component {
  @service currentUser;

  @tracked isLoading = false;

  @action
  async onFormSubmit(event) {
    event.preventDefault();
    this.isLoading = true;
    try {
      await this.args.saveCandidate(this.args.candidate);
    } finally {
      this.isLoading = false;
    }
  }
}
