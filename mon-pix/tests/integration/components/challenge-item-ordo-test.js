// import { expect } from 'chai';
// import { describe, it } from 'mocha';
// import { setupRenderingTest } from 'ember-mocha';
// import { render } from '@ember/test-helpers';
// import { hbs } from 'ember-cli-htmlbars';

// describe('Integration | Component | challenge-item-ordo', function() {
//   setupRenderingTest();

//   it('renders', async function() {
//     // Set any properties with this.set('myProperty', 'value');
//     // Handle any actions with this.set('myAction', function(val) { ... });

//     await render(hbs`<ChallengeItemOrdo />`);

//     expect(this.element.textContent.trim()).to.equal('Missing translation "pages.challenge.parts.answer-input" for locale "en-us"\n    \n      \n    \n    Missing translation "pages.ordo-proposal.title" for locale "en-us"');

//     // Template block usage:
//     await render(hbs`
//       <ChallengeItemOrdo>
//         template block text
//       </ChallengeItemOrdo>
//     `);

//     expect(this.element.textContent.trim()).to.equal('Missing translation "pages.challenge.parts.answer-input" for locale "en-us"\n    \n      \n    \n    Missing translation "pages.ordo-proposal.title" for locale "en-us"');
//   });
// });
