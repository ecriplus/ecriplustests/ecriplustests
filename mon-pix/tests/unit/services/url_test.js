import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import sinon from 'sinon';

import setupIntl from 'mon-pix/tests/helpers/setup-intl';

module('Unit | Service | locale', function (hooks) {
  setupTest(hooks);
  setupIntl(hooks);

  test('should have a frenchDomainExtension when the current domain contains pix.fr', function (assert) {
    // given
    const service = this.owner.lookup('service:url');
    service.currentDomain = { getExtension: sinon.stub().returns('fr') };

    // when
    const domainExtension = service.isFrenchDomainExtension;

    // then
    assert.true(domainExtension);
  });

  test('should not have frenchDomainExtension when the current domain contains pix.org', function (assert) {
    // given
    const service = this.owner.lookup('service:url');
    service.currentDomain = { getExtension: sinon.stub().returns('org') };

    // when
    const domainExtension = service.isFrenchDomainExtension;

    // then
    assert.false(domainExtension);
  });

  module('#homeUrl', function () {
    test('should get home url', function (assert) {
      // given
      const service = this.owner.lookup('service:url');
      service.definedHomeUrl = 'pix.test.fr';

      // when
      const homeUrl = service.homeUrl;

      // then
      const expectedDefinedHomeUrl = `${service.definedHomeUrl}?lang=${this.intl.t('current-lang')}`;
      assert.strictEqual(homeUrl, expectedDefinedHomeUrl);
    });
  });

  module('#showcaseUrl', function (hooks) {
    let defaultLocale;

    hooks.beforeEach(function () {
      defaultLocale = this.intl.t('current-lang');
    });

    hooks.afterEach(function () {
      this.intl.setLocale(defaultLocale);
    });

    [
      {
        language: 'fr',
        currentDomainExtension: 'fr',
        expectedShowcaseUrl: 'https://app.tests.ecriplus.fr/',
        expectedShowcaseLinkText: "Page d'accueil de écri+.fr",
      },
      {
        language: 'fr',
        currentDomainExtension: 'org',
        expectedShowcaseUrl: 'https://app.tests.ecriplus.fr/',
        expectedShowcaseLinkText: "Page d'accueil de écri+.org",
      },
      {
        language: 'en',
        currentDomainExtension: 'fr',
        expectedShowcaseUrl: 'https://app.tests.ecriplus.fr/en-gb',
        expectedShowcaseLinkText: "écri+.fr's Homepage",
      },
      {
        language: 'en',
        currentDomainExtension: 'org',
        expectedShowcaseUrl: 'https://app.tests.ecriplus.fr/en-gb',
        expectedShowcaseLinkText: "écri+.org's Homepage",
      },
    ].forEach(function (testCase) {
      test(`should get "${testCase.expectedShowcaseUrl}" when current domain="${testCase.currentDomainExtension}" and lang="${testCase.language}"`, function (assert) {
        // given
        const service = this.owner.lookup('service:url');
        service.definedHomeUrl = '/';
        service.currentDomain = { getExtension: sinon.stub().returns(testCase.currentDomainExtension) };
        this.intl.setLocale([testCase.language]);

        // when
        const showcase = service.showcase;

        // then
        assert.strictEqual(showcase.url, testCase.expectedShowcaseUrl);
        assert.strictEqual(showcase.linkText, testCase.expectedShowcaseLinkText);
      });
    });
  });

  module('#cguUrl', function () {
    module('when website is pix.fr', function () {
      test('returns the French page URL', function (assert) {
        // given
        const service = this.owner.lookup('service:url');
        const expectedCguUrl = 'https://ecriplus.fr/conditions-generales/';
        service.currentDomain = { getExtension: sinon.stub().returns('fr') };

        // when
        const cguUrl = service.cguUrl;

        // then
        assert.strictEqual(cguUrl, expectedCguUrl);
      });

      module('when current language is "en"', function () {
        test('returns the French page URL', function (assert) {
          // given
          const service = this.owner.lookup('service:url');
          const expectedCguUrl = 'https://ecriplus.fr/conditions-generales/';
          service.currentDomain = { getExtension: sinon.stub().returns('fr') };
          service.intl = { t: sinon.stub().returns('en') };

          // when
          const cguUrl = service.cguUrl;

          // then
          assert.strictEqual(cguUrl, expectedCguUrl);
        });
      });
    });

    module('when website is pix.org', function () {
      module('when current language is "fr"', function () {
        test('returns the French page URL', function (assert) {
          // given
          const service = this.owner.lookup('service:url');
          const expectedCguUrl = 'https://ecriplus.fr/conditions-generales/';
          service.currentDomain = { getExtension: sinon.stub().returns('org') };
          service.intl = { t: sinon.stub().returns('fr') };

          // when
          const cguUrl = service.cguUrl;

          // then
          assert.strictEqual(cguUrl, expectedCguUrl);
        });
      });

      module('when current language is "en"', function () {
        test('returns the English page URL', function (assert) {
          // given
          const service = this.owner.lookup('service:url');
          const expectedCguUrl = 'https://ecriplus.fr/conditions-generales/';
          service.currentDomain = { getExtension: sinon.stub().returns('org') };
          service.intl = { t: sinon.stub().returns('en') };

          // when
          const cguUrl = service.cguUrl;

          // then
          assert.strictEqual(cguUrl, expectedCguUrl);
        });
      });
    });
  });

  module('#dataProtectionPolicyUrl', function () {
    module('when website is pix.fr', function () {
      test('returns the French page URL', function (assert) {
        // given
        const service = this.owner.lookup('service:url');
        const expectedCguUrl = 'https://ecriplus.fr/politique-de-confidentialite/';
        service.currentDomain = { getExtension: sinon.stub().returns('fr') };

        // when
        const cguUrl = service.dataProtectionPolicyUrl;

        // then
        assert.strictEqual(cguUrl, expectedCguUrl);
      });

      module('when current language is "en"', function () {
        test('returns the French page URL', function (assert) {
          // given
          const service = this.owner.lookup('service:url');
          const expectedCguUrl = 'https://ecriplus.fr/politique-de-confidentialite/';
          service.currentDomain = { getExtension: sinon.stub().returns('fr') };
          service.intl = { t: sinon.stub().returns('en') };

          // when
          const cguUrl = service.dataProtectionPolicyUrl;

          // then
          assert.strictEqual(cguUrl, expectedCguUrl);
        });
      });
    });

    module('when website is pix.org', function () {
      module('when current language is "fr"', function () {
        test('returns the French page URL', function (assert) {
          // given
          const service = this.owner.lookup('service:url');
          const expectedCguUrl = 'https://ecriplus.fr/politique-de-confidentialite/';
          service.currentDomain = { getExtension: sinon.stub().returns('org') };
          service.intl = { t: sinon.stub().returns('fr') };

          // when
          const cguUrl = service.dataProtectionPolicyUrl;

          // then
          assert.strictEqual(cguUrl, expectedCguUrl);
        });
      });

      module('when current language is "en"', function () {
        test('returns the English page URL', function (assert) {
          // given
          const service = this.owner.lookup('service:url');
          const expectedCguUrl = 'https://ecriplus.fr/politique-de-confidentialite/';
          service.currentDomain = { getExtension: sinon.stub().returns('org') };
          service.intl = { t: sinon.stub().returns('en') };

          // when
          const cguUrl = service.dataProtectionPolicyUrl;

          // then
          assert.strictEqual(cguUrl, expectedCguUrl);
        });
      });
    });
  });
});
