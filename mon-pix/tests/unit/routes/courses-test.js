import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | courses', function (hooks) {
  setupTest(hooks);

  test('exists', function (assert) {
    const route = this.owner.lookup('route:courses');
    assert.ok(route);
  });
});
