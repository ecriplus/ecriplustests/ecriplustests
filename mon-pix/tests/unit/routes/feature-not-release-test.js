import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | feature-not-release', function (hooks) {
  setupTest(hooks);

  test('exists', function (assert) {
    const route = this.owner.lookup('route:feature-not-release');
    assert.ok(route);
  });
});
