import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import ChallengeItemGeneric from './challenge-item-generic';

export default class ChallengeItemCateg extends ChallengeItemGeneric {
  @service intl;
  answersValue = [];
  lengthProposals = 1;

  _hasError() {
    return this.answersValue.size < 1 || this.lengthProposals > 0;
  }

  _getAnswerValue() {
    return Array.from(this.answersValue).join(',');
  }

  _getErrorMessage() {
    return this.intl.t('pages.challenge.skip-error-message.categ');
  }

  @action
  answerChanged(items, lengthProposals) {
    this.answersValue = items;
    this.lengthProposals = lengthProposals;
  }
}
