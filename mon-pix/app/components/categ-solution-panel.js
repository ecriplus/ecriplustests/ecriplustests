/* eslint ember/no-classic-components: 0 */
/* eslint ember/require-computed-property-dependencies: 0 */
/* eslint ember/require-tagless-components: 0 */

import Component from '@glimmer/component';
import proposalsAsArray from 'mon-pix/utils/proposals-as-array';
export default class CategSolutionPanel extends Component {
  answer = null;
  solutionToDisplay = null;
  challenge = null;

  _getAnswer() {
    let answer;
    const rawSolution = this.args.answer.get('value');
    const pattern = /,\n,|\n,|,\n|\n/g;
    if (rawSolution.startsWith(',')) {
      answer = [''].concat(rawSolution.substr(1, rawSolution.length).split(pattern));
    } else {
      answer = this.args.answer.get('value').split(pattern);
    }
    return answer;
  }

  get responseArray() {
    const responseArray = [];
    if (this.args.solution) {
      const answer = this._getAnswer();
      const solution = this.args.solution.split('\n');
      const proposalsAndCateg = this.args.challenge.get('proposals').split('--\n');
      const categs = proposalsAsArray(proposalsAndCateg[1]);
      const proposals = proposalsAsArray(proposalsAndCateg[2]);
      for (let indexCateg = 0; indexCateg < categs.length; indexCateg++) {
        const currentArrayResponse = [];
        let answerCurrentCateg = [];
        if (answer[indexCateg]) {
          answerCurrentCateg = answer[indexCateg].split(',');
        }
        const solutionCurrentCateg = solution[indexCateg].split(',');
        const currentCateg = categs[indexCateg];
        if (answerCurrentCateg.length > 0) {
          answerCurrentCateg.map((answ) => {
            let goodness;
            const content = proposals[answ - 1].replaceAll('<br>', '');
            if (solutionCurrentCateg.indexOf(answ) !== -1 && answerCurrentCateg.indexOf(answ) !== -1) {
              goodness = true;
            } else if (solutionCurrentCateg.indexOf(answ) == -1 && answerCurrentCateg.indexOf(answ) !== -1) {
              goodness = false;
            } else {
              goodness = false;
            }
            currentArrayResponse.push({ 'content': content, 'goodness': goodness });
          });
        }
        responseArray.push({ 'name': currentCateg, 'response': currentArrayResponse });
      }
    }
    return responseArray;
  }

  get solutionArray() {
    const solutionArray = [];
    if (this.args.solution) {
      const answer = this._getAnswer();
      const solution = this.args.solution.split('\n');
      const proposalsAndCateg = this.args.challenge.get('proposals').split('--\n');
      const categs = proposalsAsArray(proposalsAndCateg[1]);
      const proposals = proposalsAsArray(proposalsAndCateg[2]);
      for (let indexCateg = 0; indexCateg < categs.length; indexCateg++) {
        const currentArraySolution = [];
        let answerCurrentCateg = [];
        if (answer[indexCateg]) {
          answerCurrentCateg = answer[indexCateg].split(',');
        }
        const solutionCurrentCateg = solution[indexCateg].split(',');
        const currentCateg = categs[indexCateg];
        solutionCurrentCateg.map((soluce) => {
          let goodness;
          const content = proposals[soluce - 1].replaceAll('<br>', '');
          if (answerCurrentCateg.indexOf(soluce) !== -1) {
            goodness = true;
          } else {
            goodness = false;
          }
          currentArraySolution.push({ 'content': content, 'goodness': goodness });
        });
        solutionArray.push({ 'name': currentCateg, 'response': currentArraySolution });
      }
    }
    return solutionArray;
  }
}
