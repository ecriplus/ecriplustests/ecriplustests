import { isNone } from '@ember/utils';
import { inject as service } from '@ember/service';
import Component from '@glimmer/component';

export default class HexagonScore extends Component {
  @service featureToggles;

  get score() {
    const score = this.args.pixScore;
    return isNone(score) || score === 0 ? '–' : Math.floor(score);
  }
}
