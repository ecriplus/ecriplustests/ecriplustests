

/* eslint ember/no-classic-components: 0 */
/* eslint ember/require-computed-property-dependencies: 0 */
/* eslint ember/require-tagless-components: 0 */

import Component from '@glimmer/component';
import proposalsAsArray from 'mon-pix/utils/proposals-as-array';


export default class OrdoSolutionPanel extends Component {
  answer = null;
  solutionToDisplay = null;
  challenge = null;

  get responseArray() {
    const responseArray = [];
    if(this.args.solution){
      const solution = this.args.solution.split(',');
      const response = this.args.answer.get('value').split(',');
      const proposals = proposalsAsArray(this.args.challenge.get('proposals'));
      if(response.length > 1 && response[1] != '#ABAND#'){
        for (let ivalue = 0; ivalue < response.length; ivalue++) {
          const currentResponse = {};
          const goodness = solution[ivalue] === response[ivalue];
          const isLast = ivalue == response.length - 1;
          const currentElement = proposals[Number(response[ivalue]) - 1].replace('<br>', '');
  
          currentResponse['goodness'] = goodness;
          currentResponse['content'] = currentElement;
          currentResponse['isLast'] = isLast;
  
          responseArray.push(currentResponse);
        }
      }
    }
    return responseArray;
  }

  get correction() {
    const correction = [];
    if(this.args.solution){
      const proposals = proposalsAsArray(this.args.challenge.get('proposals'));
      const solution = this.args.solution.split(',');
      for(const iresp of solution) {
        const currentCorrection = {};
        let currentElement = proposals[Number(iresp) - 1].replace('<br>', '');
        const isLast = solution.indexOf(iresp) == solution.length - 1;
        currentCorrection['content'] = currentElement;
        currentCorrection['isLast'] = isLast;
        correction.push(currentCorrection);  
      }
    }
    return correction;
  }
}
