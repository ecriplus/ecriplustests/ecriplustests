stages:
  - test
  - build
  - trigger

variables:
  CI_FIRST_JOB:
    value: Test
    description: "Le premier job à executer (ordre = Test/Build/Deploy/CreateRelease/LoadSeeds/DeployAirtable)"
  CI_LAST_JOB:
    value: DeployAirtable
    description: "Le dernier job à executer (ordre = Test/Build/Deploy/CreateRelease/LoadSeeds/DeployAirtable)"
  DEFAULT_OPS_BRANCH: master
  DEFAULT_CHARTS_BRANCH: master
  DEFAULT_TOOLS_BRANCH: master
  DEFAULT_HOME_BRANCH: master
  HOME_BRANCH: "3-evolution-de-la-page-d-accueil-pour-integrer-les-informations-sur-les-seeds-et-s-adapter-au"
  PROD_BRANCH: master
  DOCKER_BASE_REGISTRY: "${CI_REGISTRY}/ecriplus/ecriplustests/ecriplustests"
  DOCKER_IMAGE: "${DOCKER_BASE_REGISTRY}/ci/docker"
  NODE_ALPINE_IMAGE: "${DOCKER_BASE_REGISTRY}/ci/node_alpine"
  NODE_IMAGE: "${DOCKER_BASE_REGISTRY}/ci/node"
  DHALL_IMAGE: "${DOCKER_BASE_REGISTRY}/ci/dhall"
  HELM_IMAGE: "${DOCKER_BASE_REGISTRY}/ci/helm"
  DHALL_BIN_URL: https://github.com/dhall-lang/dhall-haskell/releases/download/1.42.0/dhall-1.42.0-x86_64-linux.tar.bz2
  STAGE_DEF: "let Stage : Type = < Test | Build | Deploy | CreateRelease | LoadSeeds | DeployAirtable > in { firstJobStage = Stage.$CI_FIRST_JOB, lastJobStage = Stage.$CI_LAST_JOB}"
  EP_PIPELINE_SOURCE: "let GitLab=$CI_PROJECT_DIR/.gitlab/ci/lib/GitLab.dhall in GitLab.PipelineSource.Type.$CI_PIPELINE_SOURCE"
  EP_CI_DEBUG: "false"
  CACHE_KEY: ""
  IS_DEPLOY: "False"
  
## Workflow rules
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /\~NOCI/ && $CI_PIPELINE_SOURCE != "web"
      when: never
    - if: $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^v.*/
      when: never
    - if: $CI_COMMIT_TAG =~ /^v.*/
      variables:
        IS_DEPLOY: "True"
      when: always
    - if: $CI_COMMIT_BRANCH == $PROD_BRANCH
      variables:
        IS_DEPLOY: "False"
      when: always
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      when: always

.values_common:
  before_script:
    - ". $CI_PROJECT_DIR/.gitlab/ci/scripts/common_values.sh"

.values_secrets:
  before_script:
    - ". $CI_PROJECT_DIR/.gitlab/ci/scripts/common_secret.sh"

## Enviromnent templates

.prod_env_template:
  environment:
    name: $PROD_BRANCH
    url: https://ecriplustests.ecriplus.fr

.dev_env_template:
  environment:
    name: rev/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.br.ecriplus.fr

## generate gitlab ci template
.generate_config_template: &generate_config_template
  stage: build
  image: "$DHALL_IMAGE"
  variables:
    GIT_STRATEGY: clone # slower but avoid cache reuse by runner which create collisions
    HOME_BRANCH: "3-evolution-de-la-page-d-accueil-pour-integrer-les-informations-sur-les-seeds-et-s-adapter-au"

  before_script:
    - !reference [.values_common, before_script]
    - !reference [.values_secrets, before_script]
  script:
    - export CACHE_KEY="$(cat package-lock.json api/package-lock.json mon-pix/package-lock.json orga/package-lock.json certif/package-lock.json admin/package-lock.json | sha256sum | cut -d' ' -f1)"
    - env
    - dhall text --file .gitlab/ci/.gitlab-ci.dhall > generated-config.yml
  artifacts:
    paths:
      - generated-config.yml

## Jobs definitions

init_docker_img:
  stage: .pre
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  # TODO: check that we do not need a tage par branch ???
  variables:
    GIT_STRATEGY: clone # slower but avoid cache reuse by runner which create collisions
    DOCKER_TAG: "${DOCKER_IMAGE}:latest"
    NODE_ALPINE_TAG: "${NODE_ALPINE_IMAGE}:latest"
    DHALL_TAG: "${DHALL_IMAGE}:latest"
    HELM_TAG: "${HELM_IMAGE}:latest"
    NODE_TAG: "${NODE_IMAGE}:latest"
  script:
    - ". $CI_PROJECT_DIR/.gitlab/ci/scripts/init_docker_img.sh"
  rules:
    - changes:
        - .gitlab/ci/*.dockerfile
        - .gitlab-ci.yml

dhall_validation:
  stage: test
  image: "$DHALL_IMAGE"
  variables:
    GIT_STRATEGY: clone # slower but avoid cache reuse by runner which create collisions
  script:
    - cd .gitlab/ci
    - find . -name \*.dhall -print | xargs -n 1 -I {} sh -c "echo \"{}\" ; diff \"{}\" <(cat \"{}\" | dhall format)"
    - find . -name \*.dhall -print | xargs -n 1 -I {} sh -c "echo \"{}\" ; diff \"{}\" <(cat \"{}\" | dhall lint)"
  rules:
    - changes:
        - .gitlab/ci/**/*.dhall
        - .gitlab-ci.yml

generate_config_prod:
  <<: *generate_config_template
  environment: !reference [.prod_env_template, environment]
  rules:
    - if: "$CI_COMMIT_TAG =~ /^v.*/"
      when: on_success

generate_config_dev:
  <<: *generate_config_template
  environment: !reference [.dev_env_template, environment]
  rules:
    - if: "$CI_COMMIT_BRANCH"
      when: on_success

child_pipeline_prod:
  stage: trigger
  needs:
    - job: generate_config_prod
      artifacts: true
      optional: false
  trigger:
    include:
      - artifact: generated-config.yml
        job: generate_config_prod
    strategy: depend
  rules:
    - if: "$CI_COMMIT_TAG =~ /^v.*/"
      when: on_success

child_pipeline_dev:
  stage: trigger
  needs:
    - job: generate_config_dev
      artifacts: true
      optional: false
  variables:
    CI_DEBUG_SERVICES: "$CI_DEBUG_SERVICES"
  trigger:
    include:
      - artifact: generated-config.yml
        job: generate_config_dev
    strategy: depend
  rules:
    - if: "$CI_COMMIT_BRANCH"
      when: on_success
