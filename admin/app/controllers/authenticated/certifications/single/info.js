import Controller from '@ember/controller';
import { alias } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { A } from '@ember/array';
import { schedule } from '@ember/runloop';
import { computed } from '@ember/object';
import cloneDeep from 'lodash/cloneDeep';
import ENV from 'pix-admin/config/environment';

export default Controller.extend({

  // Properties
  certification: alias('model'),
  edition: false,
  notifications: service('notification-messages'),
  displayConfirm: false,
  confirmMessage: '',
  confirmErrorMessage: '',
  confirmAction: 'onSave',

  // private properties
  _competencesCopy: null,
  _markStore: service('mark-store'),

  featureToggles: service('feature-toggles'),

  maxReachableLevel: () => {
    return this.featureToggles().featureToggles.maxReachableLevel;
  },

  maxReachablePixByCompetence: () => {
    return this.featureToggles().featureToggles.maxReachablePixByCompetence;
  },


  isValid: computed('certification.status', function() {
    return this.certification.status !== 'missing-assessment';
  }),

  // Actions
  actions: {
    onEdit() {
      this.set('edition', true);
    },
    onCancel() {
      this.set('edition', false);
      this.certification.rollbackAttributes();
      const competencesCopy = this._competencesCopy;
      if (competencesCopy) {
        this.set('certification.competencesWithMark', competencesCopy);
        this.set('_competencesCopy', null);
      }
    },
    onSaveConfirm() {
      const confirmMessage = 'Souhaitez-vous mettre à jour cette certification ?';
      const errors = this._getCertificationErrorsAfterJuryUpdateIfAny();
      const confirmErrorMessage = this._formatErrorsToHtmlString(errors);

      this.set('confirmMessage', confirmMessage);
      this.set('confirmErrorMessage', confirmErrorMessage);
      this.set('confirmAction', 'onSave');
      this.set('displayConfirm', true);
    },
    onCancelConfirm() {
      this.set('displayConfirm', false);
    },
    onSave() {
      const markUpdatedRequired = this._isMarksUpdatedRequired();
      this.set('displayConfirm', false);
      return this.certification.save({ adapterOptions: { updateMarks: false } })
        .then(() => {
          if (markUpdatedRequired) {
            return this.certification.save({ adapterOptions: { updateMarks: true } });
          } else {
            return Promise.resolve(true);
          }
        })
        .then(() => {
          this.notifications.success('Modifications enregistrées');
          this.set('edition', false);
          this.set('_competencesCopy', null);
        })
        .catch((e) => {
          if (e.errors && e.errors.length > 0) {
            e.errors.forEach((error) => {
              this.notifications.error(error.detail);
            });
          } else {
            this.notifications.error(e);
          }
        });
    },
    onUpdateScore(code, value) {
      this._saveCompetences();
      const existingCompetences = this.certification.competencesWithMark;
      const newCompetences = existingCompetences.map((value) => {
        return value;
      });
      const competence = newCompetences.filter((value) => {
        return (value['competence-code'] === code);
      })[0];
      if (competence) {
        if (value.trim().length === 0) {
          if (competence.level) {
            competence.score = null;
          } else {
            const index = newCompetences.indexOf(competence);
            newCompetences.splice(index, 1);
          }
        } else {
          competence.score = parseInt(value);
        }
      } else if (value.trim().length > 0) {
        newCompetences.addObject({ 'competence-code': code, 'score': parseInt(value), 'area-code': code.substr(0, 1) });
      }
      this.set('certification.competencesWithMark', newCompetences);
    },
    onUpdateLevel(code, value) {
      this._saveCompetences();
      const existingCompetences = this.certification.competencesWithMark;
      const newCompetences = existingCompetences.map((value) => {
        return value;
      });
      const competence = newCompetences.filter((value) => {
        return (value['competence-code'] === code);
      })[0];
      if (competence) {
        if (value.trim().length === 0) {
          if (competence.score) {
            competence.level = null;
          } else {
            const index = newCompetences.indexOf(competence);
            newCompetences.splice(index, 1);
          }
        } else {
          competence.level = parseInt(value);
        }
      } else if (value.trim().length > 0) {
        newCompetences.addObject({ 'competence-code': code, 'level': parseInt(value), 'area-code': code.substr(0, 1) });
      }
      this.set('certification.competencesWithMark', newCompetences);
    },
    onTogglePublishConfirm() {
      const state = this.certification.isPublished;
      if (state) {
        this.set('confirmMessage', 'Souhaitez-vous dépublier cette certification ?');
      } else {
        this.set('confirmMessage', 'Souhaitez-vous publier cette certification ?');
      }
      this.set('confirmAction', 'onTogglePublish');
      this.set('displayConfirm', true);
    },
    onTogglePublish() {
      this.set('displayConfirm', false);
      const certification = this.certification;
      const currentPublishState = certification.get('isPublished');
      let operation;
      if (currentPublishState) {
        certification.set('isPublished', false);
        operation = 'dépubliée';
      } else {
        certification.set('isPublished', true);
        operation = 'publiée';
      }
      return certification.save({ adapterOptions: { updateMarks: false } })
        .then(() => {
          this.notifications.success('Certification ' + operation);
        })
        .catch((e) => {
          this.notifications.error(e);
        });
    },
    onCheckMarks() {
      const markStore = this._markStore;
      if (markStore.hasState()) {
        const state = markStore.getState();
        const certification = this.certification;
        certification.set('pixScore', state.score);
        const newCompetences = Object.keys(state.marks).reduce((competences, code) => {
          const mark = state.marks[code];
          competences.addObject({
            'competence-code': code,
            'level': mark.level,
            'score': mark.score,
            'area-code': code.substr(0, 1),
          });
          return competences;
        }, A());
        certification.set('competencesWithMark', newCompetences);
        schedule('afterRender', this, () => {
          this.set('edition', true);
        });
      }
    },
    onUpdateCertificationBirthdate(selectedDates, lastSelectedDateFormatted) {
      this.set('certification.birthdate', lastSelectedDateFormatted);
    },
  },

  // Private methods
  _saveCompetences() {
    const copy = this._competencesCopy;
    if (!copy) {
      const current = this.certification.competencesWithMark;
      this.set('_competencesCopy', cloneDeep(current));
    }
  },

  _isMarksUpdatedRequired() {
    const {
      status, pixScore, competencesWithMark, commentForCandidate, commentForOrganization, commentForJury,
    } = this.certification.changedAttributes();
    return (
      status || pixScore || competencesWithMark || commentForCandidate || commentForOrganization || commentForJury
    );
  },

  _getCertificationErrorsAfterJuryUpdateIfAny() {
    return this._getCertificationErrorsAfterJuryUpdate(this.certification.competencesWithMark);
  },

  _getCertificationErrorsAfterJuryUpdate(competencesWithMark) {
    const errors = [];
    for (const [index, { level, score }] of competencesWithMark.entries()) {
      if (level > this.maxReachableLevel()[competencesWithMark[index].competenceId]) {
        errors.push({
          type: 'level',
          message: 'Le niveau de la compétence ' + competencesWithMark[index]['competence-code'] + ' dépasse ' + this.maxReachableLevel()[competencesWithMark[index].competenceId],
        });
      }
      if (score > this.maxReachablePixByCompetence()[competencesWithMark[index].competenceId]) {
        errors.push({
          type: 'score',
          message: 'Le nombre de point de la compétence ' + competencesWithMark[index]['competence-code'] + ' dépasse ' + this.maxReachablePixByCompetence()[competencesWithMark[index].competenceId],
        });
      }
    }

    return errors;
  },

  _formatErrorsToHtmlString(errors) {
    return errors && errors.map((err) => `${err.message}\n`).join('');
  },

});
