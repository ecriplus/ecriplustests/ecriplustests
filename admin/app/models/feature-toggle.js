import Model, { attr } from '@ember-data/model';

export default class FeatureToggle extends Model {
  @attr('number') maxReachablePixScore;
  @attr() maxReachableLevel;
  @attr() maxReachablePixByCompetence;
}
