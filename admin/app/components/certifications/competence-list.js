import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class CertificationCompetenceList extends Component {

  get competenceList() {
    return this.args.competences.map(comp => comp.index);
  }

  get indexedValues() {
    const competences = this.args.competences;
    const indexedCompetences = competences.reduce((result, value) => {
      result[value.index] = value;
      return result;
    }, {});
    const competencesList = this.competenceList;
    const scores = [];
    const levels = [];
    let index = 0;
    competencesList.forEach((value) => {
      scores[index] = indexedCompetences[value] ? indexedCompetences[value].score : null;
      levels[index] = indexedCompetences[value] ? indexedCompetences[value].level : null;
      index++;
    });
    return {
      scores: scores,
      levels: levels,
    };
  }

  @action
  onScoreChange(index, event) {
    const list = this.competenceList;
    this.args.onUpdateScore(list[index], event.target.value);
  }

  @action
  onLevelChange(index, event) {
    const list = this.competenceList;
    this.args.onUpdateLevel(list[index], event.target.value);
  }
}
