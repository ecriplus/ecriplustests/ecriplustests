Ecriplustests
===

Plateforme d'évaluation et de certification des compétences en Français écrit.


![Screenshot ecri+ App](./docs/assets/ecriplustests-app-screenshot.png)

## Présentation

Le numérique est désormais partout. Dans notre vie professionnelle comme privée. Maîtriser Internet et l'informatique
est devenu un enjeu majeur pour la société. Dans ce contexte, la mission de [Pix](https://pix.fr) est d'**aider tout un
chacun à cultiver ses compétences numériques**.

Pix s’adresse à tous : élèves, étudiants, professionnels en activité, décrocheurs, demandeurs d’emploi, séniors, ou plus
simplement citoyens qui souhaitent mesurer, développer et valoriser leurs compétences numériques.

Le service se présente sous la forme d’une [plateforme en ligne](https://app.pix.fr) d’évaluation et de certification
des compétences numériques.

Pix cherche à susciter l’envie de se former tout au long de la vie en proposant un standard ainsi que des méthodes
d’évaluation innovantes, exigeantes et bienveillantes.

## Changelog

Les changements apportés dans chaque version sont documentés dans le [CHANGELOG](./CHANGELOG.md).

## Restez en contact

- [Twitter](https://twitter.com/pix_officiel)
- [Blog](https://engineering.pix.fr)
- [Job Board](https://www.welcometothejungle.com/fr/companies/pix)

Le numérique est désormais partout. Dans notre vie professionnelle comme privée. Maîtriser Internet et l'informatique est devenu un enjeu majeur pour la société. Dans ce contexte, la mission de [ecri+](https://ecri+.fr) est d'**aider tout un chacun à cultiver ses compétences numériques**.

ecri+ s’adresse à tous : élèves, étudiants, professionnels en activité, décrocheurs, demandeurs d’emploi, séniors, ou plus simplement citoyens qui souhaitent mesurer, développer et valoriser leurs compétences numériques.

Le service se présente sous la forme d’une [plateforme en ligne](https://app.ecri+.fr) d’évaluation et de certification des compétences numériques.

ecri+ cherche à susciter l’envie de se former tout au long de la vie en proposant un standard ainsi que des méthodes d’évaluation innovantes, exigeantes et bienveillantes.

## License

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
Ce logiciel et son code source sont distribués sous [licence AGPL](https://www.gnu.org/licenses/why-affero-gpl.fr.html).
